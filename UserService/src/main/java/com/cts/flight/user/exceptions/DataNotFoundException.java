package com.cts.flight.user.exceptions;

public class DataNotFoundException  extends Exception{
	public DataNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataNotFoundException(String msg, Exception e) {
		super(msg, e);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoundException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoundException(Exception e) {
		super(e);
		// TODO Auto-generated constructor stub
	}
	
}
