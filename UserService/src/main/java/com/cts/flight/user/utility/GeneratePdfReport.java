package com.cts.flight.user.utility;

import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cts.flight.user.model.FlightBook;
import com.cts.flight.user.model.PdfDetails;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class GeneratePdfReport {
	/*
	 * @Value("${user.ticekts.pnr}") static String pnrPath;
	 */
	

	/*static String pnrPath;
	public static void bookingPdf(FlightBook flight,String pnrNum) {


		Document document = new Document();
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
	String path=	"C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\";
System.out.println("Inside pdf");
		try {
			//System.out.println("path->"+pnrPath);
			String absPath=pnrPath+"//";
			PdfWriter.getInstance(document, 
					new FileOutputStream( "C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\"+pnrNum+".pdf"));
		//	PdfWriter.getInstance(document, new FileOutputStream(absPath));
			System.out.println("abs path->"+absPath);
			document.open();
			Paragraph title = new Paragraph("BOOKING DETAILS",
					FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(20, 255, 255)));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph(" "));
			Paragraph pnr=new Paragraph("PNR  "+" "+ flight.getPnrNum());
			//pnr.setAlignment(Element.ALIGN_CENTER);
			document.add(pnr);
		//	document.add(new Paragraph(" "));
			System.out.println("para started");
			Paragraph fname=new Paragraph("AIRLINE  "+" "+ flight.getFlightName());
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fname);
			
			Paragraph fNum=new Paragraph("Flight No. "+" "+ (flight.getFlightNum()));
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fNum);
			Paragraph seat=new Paragraph("SEAT BOOKED "+" "+ (flight.getPassenger().size()));
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(seat);
			Paragraph dept=new Paragraph("DEPT TIME "+" "+ "3.30");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(dept);
			Paragraph arrt=new Paragraph("ARR. TIME "+" "+ "4.30");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(arrt);
			Paragraph pass= new Paragraph("PASSENGER DETAILS",
					FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(200, 200, 255)));
			title.setAlignment(Element.ALIGN_LEFT);
			
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(pass);
			
			  for(int i=0;i<flight.getPassenger().size();i++) {
				  System.out.println("passenger for loop");
				  Paragraph passengers=new
			  Paragraph("Name "+ flight.getPassenger().get(i).getName() +"  Gender "+ flight.getPassenger().get(i).getGender()); 
				//  Paragraph age=new
			 // Paragraph("Gender "+ flight.getPassenger().get(i).getGender());
			  passengers.setAlignment(Element.ALIGN_CENTER);
			 // age.setAlignment(Element.ALIGN_MIDDLE);
			  document.add(passengers);
			 // document.add(age);
			  }
			 
			//document.add(table);

			document.close();
			System.out.println("pdf generated");

		} catch (Exception  ex) {

		

	}*/
	
	static String pnrPath;
	public static void bookingPdf(PdfDetails pdf,String pnrNum) {


		Document document = new Document();
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
	String path=	"C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\";
System.out.println("Inside pdf");
		try {
			//System.out.println("path->"+pnrPath);
			String absPath=pnrPath+"//";
			FileOutputStream fos=new FileOutputStream("C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\"+pnrNum+".pdf");
			PdfWriter.getInstance(document, fos);
		//	PdfWriter.getInstance(document, new FileOutputStream(absPath));
			System.out.println("abs path->"+absPath);
			document.open();
			Paragraph title = new Paragraph("BOOKING DETAILS",
					FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(20, 255, 255)));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph(" "));
			Paragraph pnr=new Paragraph("PNR  "+" "+ pnrNum);
			//pnr.setAlignment(Element.ALIGN_CENTER);
			document.add(pnr);
		//	document.add(new Paragraph(" "));
			System.out.println("para started");
			Paragraph fname=new Paragraph("AIRLINE  "+" "+ pdf.getFlightName());
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fname);
			
			Paragraph fNum=new Paragraph("Flight No. "+" "+ (pdf.getFlightNum()));
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fNum);
			Paragraph seat=new Paragraph("SEAT BOOKED "+" "+ (pdf.getPassenger().size()));
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(seat);
		
			Paragraph fromPlace=new Paragraph("From "+" "+ pdf.getFrom());
			fname.setAlignment(Element.ALIGN_RIGHT);
			document.add(fromPlace);
			Paragraph to=new Paragraph("TO "+" "+ pdf.getTo());
			fname.setAlignment(Element.ALIGN_RIGHT);
			document.add(to);
			Paragraph dept=new Paragraph("DEPT TIME "+" "+ pdf.getDeptName());
			fname.setAlignment(Element.ALIGN_RIGHT);
			document.add(dept);
			Paragraph arrt=new Paragraph("ARR. TIME "+" "+ pdf.getArrTime());
			fname.setAlignment(Element.ALIGN_RIGHT);
			document.add(arrt);
			
			Paragraph pass= new Paragraph("PASSENGER DETAILS",
					FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(200, 200, 255)));
			title.setAlignment(Element.ALIGN_LEFT);
			
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(pass);
			
			  for(int i=0;i<pdf.getPassenger().size();i++) {
				  System.out.println("passenger for loop");
				  Paragraph passengers=new
			  Paragraph("Name "+ pdf.getPassenger().get(i).getName() +"  Gender "+ pdf.getPassenger().get(i).getGender()); 
				//  Paragraph age=new
			 // Paragraph("Gender "+ flight.getPassenger().get(i).getGender());
			  passengers.setAlignment(Element.ALIGN_CENTER);
			 // age.setAlignment(Element.ALIGN_MIDDLE);
			  document.add(passengers);
			 // document.add(age);
			  }
			 
			//document.add(table);

			document.close();
			fos.close();
			System.out.println("pdf generated");

		} catch (Exception  ex) {

		

	}

	//	return new ByteArrayInputStream(out.toByteArray());
	}

}
