package com.cts.flight.user.exceptions;

public class CancelTimeExceded extends Exception{

	public CancelTimeExceded() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CancelTimeExceded(String arg0, Exception arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public CancelTimeExceded(String msg, Exception arg1) {
		super(msg, arg1);
		// TODO Auto-generated constructor stub
	}

	public CancelTimeExceded(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public CancelTimeExceded(Exception msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
	

}
