package com.cts.flight.user.aop;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.cts.flight.user.exceptions.CancelTimeExceded;
import com.cts.flight.user.exceptions.DataNotFoundException;
import com.cts.flight.user.exceptions.UserNotFoundException;
import com.cts.flight.user.model.ErrorResponse;

@ControllerAdvice
public class ExceptionHandler {
	@org.springframework.web.bind.annotation.ExceptionHandler(UserNotFoundException.class)

	public ResponseEntity<ErrorResponse> handlerForUserNotFoundException(Exception e) {
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(), 404), HttpStatus.NOT_FOUND);

	}

	@org.springframework.web.bind.annotation.ExceptionHandler(CancelTimeExceded.class)
	public ResponseEntity<ErrorResponse> handlerForCancelTimeExceeded(Exception e) {
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(), 404), HttpStatus.NOT_FOUND);

	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<ErrorResponse> HandlerDataNotFoundException(Exception e) {
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(), 404), HttpStatus.NOT_FOUND);
	}
	
}
