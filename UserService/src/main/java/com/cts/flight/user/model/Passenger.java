package com.cts.flight.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Passenger implements Serializable{
	@Column(name="name")
	private String name;
	@Column(name="gender")
	private String gender;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Passenger(String name, String gender) {
		super();
		this.name = name;
		this.gender = gender;
	}
	public Passenger() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Passenger [name=" + name + ", gender=" + gender + "]";
	}

	
}
