package com.cts.flight.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cts.flight.user.model.UserRegd;
import com.cts.flight.user.repository.UserRegistrationRepository;
@Service
public class UserRegistrationService implements IUserRegistration {
	@Autowired
   private UserRegistrationRepository regdRepo;
	@Override
	public UserRegd createUser(UserRegd user) {
		// TODO Auto-generated method stub
		return regdRepo.save(user);
	}

	public boolean checkUserMailId(String mail) {
		// TODO Auto-generated method stub
		int count=regdRepo.findByMail(mail);
		if(count!=0) {
			return true;
		}
		return false;
	}

}
