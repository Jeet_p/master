package com.cts.flight.user.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cts.flight.user.exceptions.CancelTimeExceded;
import com.cts.flight.user.exceptions.DataNotFoundException;
import com.cts.flight.user.exceptions.UserNotFoundException;
import com.cts.flight.user.model.Cupon;
import com.cts.flight.user.model.FlightBook;
import com.cts.flight.user.model.FlightSearch;
import com.cts.flight.user.model.PdfDetails;
import com.cts.flight.user.repository.BookingFlightRepository;
import com.cts.flight.user.utility.GeneratePdfReport;

@Service
public class FlightBookService implements IFlightBookingService {
	@Autowired
	private BookingFlightRepository bookRepo;
	@Autowired
	private MailService mailService;
	@Autowired
	private RestTemplate template;
	@Autowired
	private GeneratePdfReport pdf;

	@Autowired
	private PdfDetails pdfDetails;

	@Override
	public String bookFlight(int flghtNum, FlightBook book) throws DataNotFoundException {
		String msg = "Flight booked with pnr num: ";
		// TODO Auto-generated method stub
		// generating random pnr number
		String pnr = generateRandomPnr();
		// consuming flight service to gte flight name and cost
		ResponseEntity<FlightSearch> res = getFlightDetails(flghtNum);
		FlightSearch flight = res.getBody();
		if (flight != null && flight.getFlyingStatus().equalsIgnoreCase("flying")) {
			System.out.println("generated pnr->" + pnr);
			book.setPnrNum(pnr);
			book.setFlightNum(flghtNum);
			book.setFlightName(flight.getAirlineName());
			// applied cupon cost
			if (book.getCuponCode() != null) {
				double cuponCost = applyCupons(book);
				book.setCost(cuponCost);
			} else {
				book.setCost(applyCupons(book));
				book.setCuponCode("no cupon applied");
			}

			// pdfDetails.setPnrNum(pnr);
			pdfDetails.setFlightName(flight.getAirlineName());
			pdfDetails.setFlightNum(flghtNum);
			pdfDetails.setSeats(book.getPassenger().size());
			pdfDetails.setDeptName(flight.getDepTime());
			pdfDetails.setArrTime(flight.getArrTime());
			pdfDetails.setPassenger(book.getPassenger());
			pdfDetails.setFrom(flight.getFromPlace());
			pdfDetails.setTo(flight.getToPlace());

			bookRepo.save(book);
			System.out.println("pdf pnr->" + pnr);
			GeneratePdfReport.bookingPdf(pdfDetails, book.getPnrNum());
			msg = msg + pnr;
			try {
				mailService.sendEmail(book.getMailId(), msg);
			} catch (Exception e) {
				msg = "Some technical issue while sending mail,check your booking history to see confirmation";
			}
		} else {
			msg = "Flight is not available to book or its blocked " + flghtNum;
			throw new DataNotFoundException(msg);
		}
		return msg;
	}

	/*
	 * @Override public String bookFlight(int flghtNum, FlightBook book) { String
	 * msg = "Flight booked with pnr num: "; // TODO Auto-generated method stub
	 * String pnr = generateRandomPnr(); book.setPnrNum(pnr);
	 * book.setFlightNum(flghtNum); String flightName = getFlightName(flghtNum);
	 * book.setFlightName(flightName); ByteArrayInputStream
	 * bytes=pdf.bookingPdf(book); GeneratePdfReport.bookingPdf(book,
	 * book.getPnrNum()); book.setPnrPdf(bytes); bookRepo.save(book); msg = msg +
	 * pnr; try { mailService.sendEmail(book.getMailId(), msg); } catch (Exception
	 * e) { msg =
	 * "Some technical issue while sending mail,check your booking history to see confirmation"
	 * ; } return msg; }
	 */

	public String generateRandomPnr() {
		int leftLimit = 65; // letter 'A'
		int rightLimit = 90; // letter 'Z'
		int targetStringLength = 10;
		Random random = new Random();

		String generatedString = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

		System.out.println(generatedString);
		return generatedString;

	}

	@Override
	public List<FlightBook> getBookedFlightsByMailId(String mailId) {
		// TODO Auto-generated method stub
		if (mailId != null && mailId != "" && !mailId.isEmpty()) {
			return (bookRepo.findByMailId(mailId));
		}
		/*
		 * else { String msg="Mail Id "+ mailId + " not  " }
		 */
		return null;

	}

	@Override
	public FlightBook getTicketDetailsByPnr(String pnr) throws UserNotFoundException {
		// TODO Auto-generated method stub

		if (bookRepo.existsFlightBookByPnrNum(pnr)) {
			FlightBook flight = bookRepo.findByPnrNum(pnr);
			return flight;
		} else {
			String msg = "PNR number " + pnr + " is incorrect";
			throw new UserNotFoundException(msg);
		}

	}

	public ResponseEntity<FlightSearch> getFlightDetails(int flightNum) throws DataNotFoundException {
		System.out.println("inside flight name get");
		String url = "http://localhost:9091/app/v1.0/flight/airlline/flightdeatils/" + flightNum;
		System.out.println(url);
		try {
			ResponseEntity<FlightSearch> res = template.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<FlightSearch>() {
					});
			if (res.getBody() != null) {

				return res;
			} else {
				String msg = "Flight is not available to book or blocked " + flightNum;
				throw new DataNotFoundException(msg);
			}

		} catch (Exception d) {
			String msg = "Flight is not available to book or blocked " + flightNum;

			throw new DataNotFoundException(msg);
		}

	}

	@Transactional
	@Override
	public String cancelTickets(String pnr) throws CancelTimeExceded, UserNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("inside cancel tickets");
		String msg = "";
		int number;
		FlightBook flight = bookRepo.findByPnrNum(pnr);
		if (flight != null) {
			number = flight.getFlightNum();
			System.out.println("cancel->" + number);
			String url = "http://localhost:9091/app/v1.0/flight/airlline/flightdeatils/" + number;
			System.out.println("cancel uri->" + url);
			ResponseEntity<FlightSearch> res = template.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<FlightSearch>() {
					});
			String date = res.getBody().getStartDate();
			System.out.println("cancel date->+" + date);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDateTime now = LocalDateTime.now();
			String currdate = dtf.format(now);
			System.out.println(currdate);
			System.out.println(date);
			long diffHour = getDuration(currdate, date);
			System.out.println("diff hour->" + diffHour);
			if (diffHour > 24) {
				bookRepo.deleteByPnrNum(pnr);
				System.out.println("diff hour->" + diffHour);
				msg = "Flight cancelled for pnr " + pnr;
			} else {
				msg = "You can not cancel ticket before 24 hr of flying date";
				throw new CancelTimeExceded(msg);
			}
		} else {
			msg = "No booking history for pnr number " + pnr;
			throw new UserNotFoundException(msg);
		}

		return msg;
	}

	public long getDuration(String start_date, String end_date) {
		long difference_In_Hours = 0;

		System.out.println("start->" + start_date);
		System.out.println("end->" + end_date);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		try {

			// parse method is used to parse
			// the text from a string to
			// produce the date
			Date d1 = sdf.parse(start_date);
			Date d2 = sdf.parse(end_date);

			// Calucalte time difference
			// in milliseconds
			long difference_In_Time = d2.getTime() - d1.getTime();

			// Calucalte time difference in
			// seconds, minutes, hours, years,
			// and days
			long difference_In_Seconds = (difference_In_Time / 1000) % 60;

			long difference_In_Minutes = (difference_In_Time / (1000 * 60)) % 60;

			/*
			 * long difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
			 */

			long difference_In_Years = (difference_In_Time / (1000l * 60 * 60 * 24 * 365));

			long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
			difference_In_Hours = difference_In_Days * 24;

		}

		// Catch the Exception
		catch (ParseException e) {
			e.printStackTrace();
		}

		return difference_In_Hours;

	}

	public String getFlightName(int flightNum) {
		System.out.println("inside flight name get");
		String url = "http://localhost:9091/app/v1.0/flight/airlline/getname/" + flightNum;
		System.out.println(url);
		ResponseEntity<String> res = template.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return res.getBody();

	}

	@Override
	public List<Cupon> viewCupon() throws DataNotFoundException {
		System.out.println("inside flight name get");
		String url = "http://localhost:9091/app/v1.0/flight/admin/cupons";
		System.out.println(url);
		ResponseEntity<List<Cupon>> res = template.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Cupon>>() {
				});
		List<Cupon> cupons = res.getBody();
		if (cupons != null) {
			return cupons;
		} else {

			throw new DataNotFoundException("No cupons to Apply");
		}

	}

	@Override

	public double getFlightCost(FlightBook book) throws DataNotFoundException {
		if (book != null) {
			int number = book.getFlightNum();

			String url = "http://localhost:9091/app/v1.0/flight/airlline/flightdeatils/" + number;

			ResponseEntity<FlightSearch> res = template.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<FlightSearch>() {
					});
			if (res.getBody() != null) {
				double actualCost = res.getBody().getCost();
				// get cupon code and fecth cupon discount
				return actualCost;
			} else {
				String msg = "Flight not available to book " + book.getFlightNum();
				throw new DataNotFoundException(msg);
			}

		}
		return 0;

	}

	@Override
	public double applyCupons(FlightBook book) throws DataNotFoundException {
		System.out.println("inside flight name get");
		String cp = book.getCuponCode();
		String url = "http://localhost:9091/app/v1.0/flight/admin/getcupon/" + cp;
		System.out.println(url);
		if (book.getCuponCode() != null) {
			ResponseEntity<Cupon> res = template.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<Cupon>() {
					});
			if (res.getBody() != null) {
				double discointCost = res.getBody().getDiscount();
				double actualCost = (getFlightCost(book) * book.getSeats()) - discointCost;
				return actualCost;

			} else {
				String msg = "Cupon data not available";
				throw new DataNotFoundException(msg);
			}
		} else {
			double ticketCost = getFlightCost(book) * book.getSeats();
			return ticketCost;

		}
		// return 0;

	}

}
