package com.cts.flight.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cts.flight.user.model.FlightBook;

public interface BookingFlightRepository extends JpaRepository<FlightBook, String> {
	public List<FlightBook> findByMailId(String mailId);
	public boolean existsFlightBookByPnrNum(String pnr);
	public FlightBook findByPnrNum(String pnrNum);
	//@Query("delete  from  FlightBook where pnrNum = :pnrNum" )
	public void deleteByPnrNum(String pnrNum);

}
