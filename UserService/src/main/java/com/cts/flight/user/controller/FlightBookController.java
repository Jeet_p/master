package com.cts.flight.user.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cts.flight.user.exceptions.CancelTimeExceded;
import com.cts.flight.user.exceptions.DataNotFoundException;
import com.cts.flight.user.exceptions.UserNotFoundException;
import com.cts.flight.user.model.Cupon;
import com.cts.flight.user.model.ErrorResponse;
import com.cts.flight.user.model.FlightBook;
import com.cts.flight.user.model.FlightSearch;
import com.cts.flight.user.service.IFlightBookingService;

import io.swagger.annotations.Api;

@RestController
@Api
public class FlightBookController {
	@Autowired
	private IFlightBookingService service;
	@Autowired
	private RestTemplate template;

	@PostMapping("/book/{flightnum}")
	public ResponseEntity<ErrorResponse> bookFlight(@PathVariable int flightnum, @RequestBody FlightBook book) throws DataNotFoundException {
		String msg = service.bookFlight(flightnum, book);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200), HttpStatus.CREATED);

	}

	@GetMapping("/history/{mailId}")
	public ResponseEntity<List<FlightBook>> bookingHistoryByMailId(@PathVariable String mailId) {
		List<FlightBook> books = service.getBookedFlightsByMailId(mailId);
		return new ResponseEntity<List<FlightBook>>(books, HttpStatus.OK);

	}

	@GetMapping("/search/{startDate}/{fromPlace}/{toPlace}/{trip}")
	public List<FlightSearch> searchFlight(@PathVariable String startDate, @PathVariable String fromPlace,
			@PathVariable String toPlace, @PathVariable String trip) throws DataNotFoundException {

		System.out.println("user search");
		String baseUrl = "http://localhost:9091/app/v1.0/flight/airlline";
		baseUrl = baseUrl + "//search" + "//" + startDate + "//" + fromPlace + "//" + toPlace + "//" + trip;
		ResponseEntity<List<FlightSearch>> resp = template.exchange(baseUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FlightSearch>>() {
				});
		if(resp.getBody()!=null) {
		System.out.println(resp.getBody());

		return resp.getBody();
		}
		else {
			throw new DataNotFoundException("Flight not available on searcgung details");
		}

	}

	@GetMapping("/details/{pnr}")
	public ResponseEntity<FlightBook> getBookingDetailsByPnr(@PathVariable String pnr) throws UserNotFoundException {
		System.out.println("PNR->" + pnr);
		FlightBook book = service.getTicketDetailsByPnr(pnr);
		return new ResponseEntity<FlightBook>(book, HttpStatus.OK);

	}

	@GetMapping("/cancel/{pnr}")
	public ResponseEntity<ErrorResponse> cancelTickets(@PathVariable String pnr)
			throws CancelTimeExceded, UserNotFoundException {
		String msg = service.cancelTickets(pnr);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200), HttpStatus.OK);

	}

	@GetMapping("/download/{pnrnum}")
	public ResponseEntity<InputStreamResource> downloadPdf(@PathVariable String pnrnum) throws DataNotFoundException {
		String path = "C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\"
				+ pnrnum + ".pdf";
			File file = new File(path);
			
			if(file.exists()) {
			HttpHeaders respHeaders = new HttpHeaders();
			MediaType mediaType = MediaType.parseMediaType("application/pdf");
			respHeaders.setContentType(mediaType);
			respHeaders.setContentLength(file.length());
			respHeaders.setContentDispositionFormData("attachment", file.getName());
			InputStreamResource isr;
			try {
				isr = new InputStreamResource(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				String msg="ticket deatils are not there to downloade,incorrect PNR number";
				throw new DataNotFoundException(msg);
			}
			return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
			}else {
				String msg="ticket deatils are not there to downloade,incorrect PNR number";
				throw new DataNotFoundException(msg);
			}
	
			

			// return new
			// ResponseEntity<InputStreamResource>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
			//return null;
	

	@GetMapping("/viwecupons")
	public ResponseEntity<List<Cupon>> vieCupon() throws DataNotFoundException {
		List<Cupon> cupons=service.viewCupon();
		return new ResponseEntity<List<Cupon>>(cupons,HttpStatus.OK);

	}


}
