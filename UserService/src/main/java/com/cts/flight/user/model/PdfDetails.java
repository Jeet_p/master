package com.cts.flight.user.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;
@Component
public class PdfDetails implements Serializable{

	private String pnrNum;
	
	private String name;
	
	private String mailId;

	private List<Passenger> passenger;
	private String mealType;
	private int seats;

	private int flightNum;
	private String flightName;
	private String deptName;
	private String arrTime;
	private String flyingDate;
	private String from;
	private String to;
	public String getPnrNum() {
		return pnrNum;
	}
	public void setPnrNum(String pnrNum) {
		this.pnrNum = pnrNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public List<Passenger> getPassenger() {
		return passenger;
	}
	public void setPassenger(List<Passenger> passenger) {
		this.passenger = passenger;
	}
	public String getMealType() {
		return mealType;
	}
	public void setMealType(String mealType) {
		this.mealType = mealType;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public int getFlightNum() {
		return flightNum;
	}
	public void setFlightNum(int flightNum) {
		this.flightNum = flightNum;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getFlyingDate() {
		return flyingDate;
	}
	public void setFlyingDate(String flyingDate) {
		this.flyingDate = flyingDate;
	}
	@Override
	public String toString() {
		return "PdfDetails [pnrNum=" + pnrNum + ", name=" + name + ", mailId=" + mailId + ", passenger=" + passenger
				+ ", mealType=" + mealType + ", seats=" + seats + ", flightNum=" + flightNum + ", flightName="
				+ flightName + ", deptName=" + deptName + ", arrTime=" + arrTime + ", flyingDate=" + flyingDate + "]";
	}
	public PdfDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PdfDetails(String pnrNum, String name, String mailId, List<Passenger> passenger, String mealType, int seats,
			int flightNum, String flightName, String deptName, String arrTime, String flyingDate) {
		super();
		this.pnrNum = pnrNum;
		this.name = name;
		this.mailId = mailId;
		this.passenger = passenger;
		this.mealType = mealType;
		this.seats = seats;
		this.flightNum = flightNum;
		this.flightName = flightName;
		this.deptName = deptName;
		this.arrTime = arrTime;
		this.flyingDate = flyingDate;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}

}
