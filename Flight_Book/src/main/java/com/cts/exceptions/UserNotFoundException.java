package com.cts.exceptions;

public class UserNotFoundException extends Exception{

	public UserNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserNotFoundException(String msg, Exception e) {
		super(msg, e);
		// TODO Auto-generated constructor stub
	}

	public UserNotFoundException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public UserNotFoundException(Exception e) {
		super(e);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
