package com.cts.aop;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;


import com.cts.exceptions.UserNotFoundException;
import com.cts.model.ErrorResponse;

@ControllerAdvice
public class ExceptionHandler {
@org.springframework.web.bind.annotation.ExceptionHandler(UserNotFoundException.class)

public ResponseEntity<ErrorResponse> handlerForUserNotFoundException(Exception e){
	return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(),404),HttpStatus.NOT_FOUND);
	
}
}
