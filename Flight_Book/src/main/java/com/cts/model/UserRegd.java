package com.cts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_details")
public class UserRegd {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	@Column(name="FIRSTNAME")
	private String firstName;
	@Column(name="LASTNAME")
	private String lastName;
	@Column(name="CONTACT")
	private double mobNumber;
	@Column(name="MAILID")
	private String mailId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public UserRegd() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRegd(int userId, String firstName, String lastName, double mobNumber, String mailId) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobNumber = mobNumber;
		this.mailId = mailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getMobNumber() {
		return mobNumber;
	}
	public void setMobNumber(double mobNumber) {
		this.mobNumber = mobNumber;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	@Override
	public String toString() {
		return "UserRegd [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", mobNumber="
				+ mobNumber + ", mailId=" + mailId + "]";
	}


}
