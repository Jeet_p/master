package com.cts.services;

import com.cts.model.UserRegd;

public interface IUserRegistration {
	public UserRegd createUser(UserRegd user);
	public boolean  checkUserMailId(String mail);

}
