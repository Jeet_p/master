package com.cts.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cts.exceptions.UserNotFoundException;
import com.cts.model.Admin;
import com.cts.repository.AdminRepository;

@Service
public class AdminService {
	@Autowired
	private AdminRepository adminRepo;

	public Admin createAdmin(Admin admin) {
		return adminRepo.save(admin);

	}

	public String isValidAdmin(Admin admin) throws UserNotFoundException {
		
		String msg="";
			int count = adminRepo.validateAdmin(admin.getUserName(), admin.getPassword());
			if (count != 0) {
				msg="successfully loged in as admin";
			} else {
				throw new UserNotFoundException("You are not authorized as Admin,check with Admin people");
			}

		
		return msg;
	}

}
