package com.cts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cts.model.UserRegd;

public interface UserRegistrationRepository extends JpaRepository<UserRegd, Integer> {
	@Query("select count(*) from UserRegd where mailId = :mail")
	public int findByMail(String mail);

}
