package com.cts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cts.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, String> {
	@Query("SELECT COUNT(*) FROM Admin WHERE userName= :userName AND password= :password")
	public int validateAdmin(@Param("userName") String userName,@Param("password") String password);

}
