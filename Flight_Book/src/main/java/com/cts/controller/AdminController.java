package com.cts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.exceptions.UserNotFoundException;
import com.cts.model.Admin;
import com.cts.services.AdminService;

@RestController
@RequestMapping("/app/v1.0/flight/admin")
public class AdminController {
	@Autowired
	private AdminService adminService;

	@RequestMapping("/create")
	public ResponseEntity<String> createAdmin(@RequestBody Admin admin) {
		Admin ad = adminService.createAdmin(admin);
		if (ad != null) {
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}

		else {
			return new ResponseEntity<String>("Admin not created", HttpStatus.OK);
		}

	}

	@PostMapping("/validate")
	public ResponseEntity<String> validateAdmin(@RequestBody Admin admin) throws UserNotFoundException {
	
			String validMsg = adminService.isValidAdmin(admin);
			return new ResponseEntity<String>(validMsg, HttpStatus.OK);
		
		}

	}


