package com.cts.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.model.UserRegd;
import com.cts.services.IOtpService;
import com.cts.services.IUserRegistration;
import com.cts.services.MailService;


@RestController
@RequestMapping("/app/v1.0/flight/user")
public class UserRegistrationController {
	@Autowired
	private IUserRegistration userService;
	@Autowired
	private IOtpService otpService;
	@Autowired
	private MailService mailService;
	@RequestMapping("/saveUser")
	public ResponseEntity<UserRegd> sighnUp(@RequestBody UserRegd regd){
		UserRegd user=userService.createUser(regd);
		return new ResponseEntity<UserRegd>(regd,HttpStatus.CREATED);
		//return null;
		
	}
	@RequestMapping("/login/{mail}")
	public ResponseEntity<String> userLogin(@PathVariable String mail) {
		System.out.println("login coontroler->"+mail);
	
		otpService.generateOTP(mail);
		String msg="Continue Loging in for CTS flight book By Entering The Below Code";
		boolean flag=userService.checkUserMailId(mail);
		if(flag==true) {
		int otp=otpService.getOtp(mail);
		String otpMsg=msg+" " +otp;
		mailService.sendEmail(mail, otpMsg);
		System.out.println("otp sent to mail=-> "+ mail + "with otp-> " + otp);
		return new ResponseEntity<String>("Otp Sent to mail id for login",HttpStatus.OK);
		
		}
		else {
			System.out.println("user does not exist->"+ mail);
			return new ResponseEntity<String>("User does not exist,please sign up",HttpStatus.FORBIDDEN);
		}
		
	}

	@RequestMapping("/validate/{mail}/{otp}")
	public ResponseEntity<String> validateUserLogin(@PathVariable String mail,@PathVariable int otp) {
		String msg="";
		int otp1=otpService.getOtp(mail);
		boolean flag=otpService.validateOtp(otp1, otp);
		if(flag==true) {
			msg="Successfully loged In";
			otpService.clearOTP(mail);
		}
		else {
			msg="Invalid code Entered";
		}
		return new ResponseEntity<String>(msg,HttpStatus.FORBIDDEN);

	}
}
