package com.example.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class GeneratePdfReport {
	  @Value("${user.ticekts.pnr}")
	     static String path;

	public static void bookingPdf() {

		Document document = new Document();
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.out.println(path);
		try {
			PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\booking1.pdf"));
			document.open();
			PdfPTable table = new PdfPTable(8);
			table.setWidthPercentage(100);
			table.setWidths(new int[] { 5, 5, 5, 5, 5, 5, 5, 5 });

			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			//document.add(new Paragraph("BOOKING DETAILS").);
			//document.add(new Paragraph("BOOKING DETAILS"));
			Paragraph title = new Paragraph("BOOKING DETAILS",
					FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(20, 255, 255)));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph(" "));
			Paragraph pnr=new Paragraph("PNR NUMB "+" "+ "123445");
			//pnr.setAlignment(Element.ALIGN_CENTER);
			document.add(pnr);
		//	document.add(new Paragraph(" "));
			Paragraph fname=new Paragraph("AIRLINE NAME "+" "+ "Indigo");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fname);
			
			Paragraph fNum=new Paragraph("Flight Number "+" "+ "123455");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(fNum);
			Paragraph seat=new Paragraph("SEAT BOOKED "+" "+ "4");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(seat);
			Paragraph dept=new Paragraph("DEPT TIME "+" "+ "2.30");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(dept);
			Paragraph arrt=new Paragraph("ARR. TIME "+" "+ "3.30");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(arrt);
			Paragraph pass=new Paragraph("passenger detaiils ");
			fname.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(pass);
			
			
			
			
			
			
			  table.setSpacingBefore(25); 
			  table.setSpacingAfter(25);
	
			  
			

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("FLIGHT PNR", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			hcell = new PdfPCell(new Phrase("FLIGHT NAME", headFont));
			table.addCell(hcell);
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			hcell = new PdfPCell(new Phrase("FLIGHT NUMBER", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			hcell = new PdfPCell(new Phrase("EMAIL", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			hcell = new PdfPCell(new Phrase("PASSENGER COUNT", headFont));
			table.addCell(hcell);
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			hcell = new PdfPCell(new Phrase("MEAL TYPE", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);
			/*
			 * hcell = new PdfPCell(new Phrase("PASSENGERS", headFont));
			 * hcell.setHorizontalAlignment(Element.ALIGN_CENTER); table.addCell(hcell);
			 */

			

				PdfPCell cell;

				cell = new PdfPCell(new Phrase(12345));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase("Indigo".toString()));
				cell.setPaddingLeft(5);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(String.valueOf(234566)));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setPaddingRight(5);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("pporirrhrrr"));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(String.valueOf(5)));
				cell.setPaddingLeft(5);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase("veg"));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setPaddingRight(5);
				table.addCell(cell);
				/*
				cell = new PdfPCell(new Phrase(flight.getPassenger().get));
			
			 * cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			 * cell.setHorizontalAlignment(Element.ALIGN_RIGHT); cell.setPaddingRight(5);
			 * table.addCell(cell);
			 */
			


			document.add(table);

			document.close();

		} catch (Exception  ex) {

		

	}

	//	return new ByteArrayInputStream(out.toByteArray());
	}

}
