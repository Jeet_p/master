package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class Proj1Springboot1Applicatio implements CommandLineRunner{


	public static void main(String[] args) throws ParseException {
		SpringApplication.run(Proj1Springboot1Applicatio.class, args);
       System.out.println("spring main");

	String date="15-09-2021";
   	SimpleDateFormat sdf=new SimpleDateFormat("dd-mm-yyyy");
    
    	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	   LocalDateTime now = LocalDateTime.now();
    	   String currdate=dtf.format(now);
    	   System.out.println(currdate);
    	   System.out.println(date);
    	   findDifference(currdate,date);
		/*
		 * Date gdate=new SimpleDateFormat("dd-mm-yyyy").parse(date); //
		 * System.out.println("Current date: "+str); //Date date1=sdf.parse(date);
		 * System.out.println(gdate);
		 */
	

	//cal.add(Calendar.DAY_OF_MONT);
	//System.out.println(sdf.format(cal.getTime()));
	

	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	   static void
	    findDifference(String start_date,
	                   String end_date)
	    {
		   System.out.println("start->"+ start_date);
		   System.out.println("end->"+ end_date);
	  
	        // SimpleDateFormat converts the
	        // string format to date object
	        SimpleDateFormat sdf
	            = new SimpleDateFormat(
	                "dd-MM-yyyy");
	  
	        // Try Block
	        try {
	  
	            // parse method is used to parse
	            // the text from a string to
	            // produce the date
	            Date d1 = sdf.parse(start_date);
	            Date d2 = sdf.parse(end_date);
	  
	            // Calucalte time difference
	            // in milliseconds
	            long difference_In_Time
	                = d2.getTime() - d1.getTime();
	  
	            // Calucalte time difference in
	            // seconds, minutes, hours, years,
	            // and days
	            long difference_In_Seconds
	                = (difference_In_Time
	                   / 1000)
	                  % 60;
	  
	            long difference_In_Minutes
	                = (difference_In_Time
	                   / (1000 * 60))
	                  % 60;
	  
			/*
			 * long difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
			 */
	  
	            long difference_In_Years
	                = (difference_In_Time
	                   / (1000l * 60 * 60 * 24 * 365));
	  
	            long difference_In_Days
	                = (difference_In_Time
	                   / (1000 * 60 * 60 * 24))
	                  % 365;
	            long difference_In_Hours=difference_In_Days*24;
	  
	            // Print the date difference in
	            // years, in days, in hours, in
	            // minutes, and in seconds
	  
	            System.out.print(
	                "Difference "
	                + "between two dates is: ");
	  
	            System.out.println(
	                difference_In_Years
	                + " years, "
	                + difference_In_Days
	                + " days, "
	                + difference_In_Hours
	                + " hours, "
	                + difference_In_Minutes
	                + " minutes, "
	                + difference_In_Seconds
	                + " seconds");
	        }
	  
	        // Catch the Exception
	        catch (ParseException e) {
	            e.printStackTrace();
	        }
	    }

}
