import { FlyingDates } from "./flying-dates";

export class Flights {
	
	constructor(){
		
	}

    public  flightNum:any="";
	
	public  airlineName :any;
	
	public  fromPlace: any;
	
	public  toPlace:any;

	public  startDate:any;
	
	public  endDate:any;
	
	public  depTime:any;
	
	public  arrTime:any;
	public  instrument:any;
	
	public  buisnesSeat:any
	
	public  nonBuisnessSeat:any;
	public  cost:any;

	public  flyingStatus:any;
	
	public  rows:any;
	public  mealType:any;
 
	public flyingDates:FlyingDates[]=[];
	
}
