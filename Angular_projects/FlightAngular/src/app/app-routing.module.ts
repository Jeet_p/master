import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserloginComponent } from './components/userlogin/userlogin.component';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';

const routes: Routes = [
{ path:'user',  component:UserloginComponent},
{ path:'admin', component:AdminloginComponent },
{ path: "adminsucess", loadChildren: ()=>import("./module/admin/admin.module").then(M=>M.AdminModule)},

{ path: "usersuccess", loadChildren: ()=>import("./module/user/user.module").then(M=>M.UserModule)},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
