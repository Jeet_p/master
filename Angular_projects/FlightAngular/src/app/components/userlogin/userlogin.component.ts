import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.scss']
})
export class UserloginComponent implements OnInit {
  mail:any="";
  otp:any="";

  constructor(private router:Router,private userService:UserServiceService) { }

  ngOnInit(): void {
  }
  getOtp(){
    
    this.userService.generateOtp(this.mail).subscribe((res:any)=>{
alert(res.msg);

    });
  }
  getlogin(){
    this.userService.userValidate(this.mail,this.otp).subscribe((res:any)=>{
      alert(res.msg);
      localStorage.setItem("user",this.mail);
      this.router.navigate(["/usersuccess"]);
    });
 
  }
}
