import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from 'src/app/model/admin';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.scss']
})
export class AdminloginComponent implements OnInit {


  constructor(private adminservice: AdminServiceService,private router:Router) { }

username: string="";
password: string="";
msg:string="";


  ngOnInit() {
  }

  login() : void {
    let admin=new Admin(this.username,this.password);
    this.adminservice.authenticate(admin).subscribe((res:any)=>{
      console.log(res);
      this.msg = res.token;
      if(!!this.msg){
        localStorage.setItem("token",this.msg);
        alert(this.msg);
        alert("successfully LogedIn");
        this.router.navigate(["/adminsucess"]);
    
      }else{
        alert("Invalid Credentials");
      }
  })

  }
  }


