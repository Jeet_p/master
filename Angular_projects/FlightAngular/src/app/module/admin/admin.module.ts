import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlightsComponent } from './components/flights/flights.component';

import { RescheduleComponent } from './components/reschedule/reschedule.component';
import { CuponsComponent } from './components/cupons/cupons.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CreateComponent } from './components/create/create.component';
import { CustommaterialmoduleModule } from '../custommaterialmodule/custommaterialmodule.module';
import { FormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { ManagecuponsComponent } from './components/managecupons/managecupons.component';
import { LogoutComponent } from './components/logout/logout.component';



@NgModule({
  declarations: [
   CreateComponent,
    FlightsComponent,
  
    RescheduleComponent,
    CuponsComponent,
   
    HomeComponent,
     ManagecuponsComponent,
     LogoutComponent
  ],
  imports: [
    FormsModule,
    CustommaterialmoduleModule,
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
