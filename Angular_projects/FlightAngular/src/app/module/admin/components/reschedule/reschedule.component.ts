import { Component, OnInit } from '@angular/core';
import { Flights } from 'src/app/model/flights';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-reschedule',
  templateUrl: './reschedule.component.html',
  styleUrls: ['./reschedule.component.scss']
})
export class RescheduleComponent implements OnInit {
  dtime:any="";
  atime:any="";
  sdate:any="";
  edate:any="";
  msg:any="";
  fnumber:any="";
  flight=new Flights();
  constructor(public admin:AdminServiceService) { }

  ngOnInit(): void {
  }
  update(){
    
   debugger
   this. flight.depTime=this.dtime;
    this.flight.arrTime=this.atime;
   this. flight.startDate=this.sdate;
   this. flight.endDate=this.edate;
   
   if(!!this.flight.depTime  && !!this.flight.arrTime &&!! this.flight.startDate &&  !!this. flight.endDate){
  //  this.admin.update();
    this.admin.updateFlight(this.flight,this.fnumber).subscribe((res:any)=>{
      this.msg=res.msg;
      alert(this.msg);
      this. flight.depTime="";
      this.flight.arrTime="";
      this. flight.startDate="";
       this. flight.endDate="";

    });
   
  }
  else{
    alert("Fill the required data")
  }
    // this.admin.viewCupons();
  }

}
