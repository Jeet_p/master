import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cupon } from 'src/app/model/cupon';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-cupons',
  templateUrl: './cupons.component.html',
  styleUrls: ['./cupons.component.scss']
})
export class CuponsComponent implements OnInit {

  constructor(private  adminServiice:AdminServiceService,private router:Router) { }
  code: string="";
  discount: string="";
 msg:string="";


  ngOnInit() {
  }

  create() : void {
    if(!!this.code && !!this.discount){
      let cupon=new Cupon(this.code,this.discount);

      this.adminServiice.createCupon(cupon).subscribe((res:any)=>{
      
      alert(res.msg);
      this.code="";
      this.discount="";
        // if(!!this.msg){
        //   alert(this.msg);
        //   alert("cupon created");
        // }
    
        
      });

    }
    else{
      alert("Cupon details should not be empty")
    }
//  let cupon=new Cupon(this.code,this.discount);

//   this.adminServiice.createCupon(cupon).subscribe((res:any)=>{
  
//   alert(res.msg);
//     // if(!!this.msg){
//     //   alert(this.msg);
//     //   alert("cupon created");
//     // }

    
//   });

  }

}
