import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Cupon } from 'src/app/model/cupon';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-managecupons',
  templateUrl: './managecupons.component.html',
  styleUrls: ['./managecupons.component.scss']
})
export class ManagecuponsComponent implements OnInit {
  cupons:Cupon[]=[];

  constructor(private adminservice:AdminServiceService,private route:Router) { }

  ngOnInit(): void {
  this.getAllCupons();
  }
  
  getAllCupons(){
 this.adminservice.viewCupons().subscribe((res:any)=>{
 this.cupons=res;
 });
  }
  modifyCupon(code:any){
   this.route.navigateByUrl("/adminsucess/cupons");
  }

}
