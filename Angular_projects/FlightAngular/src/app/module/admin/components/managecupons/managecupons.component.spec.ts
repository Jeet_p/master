import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagecuponsComponent } from './managecupons.component';

describe('ManagecuponsComponent', () => {
  let component: ManagecuponsComponent;
  let fixture: ComponentFixture<ManagecuponsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagecuponsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagecuponsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
