import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Flights } from 'src/app/model/flights';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {
  flights:any=[];
  msg:any;
  constructor(private adminservice:AdminServiceService,private route:Router) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll(){
    console.log("in gett all flights");
    this.adminservice.getAllflights().subscribe((res:any)=>{
      console.log(res);
  
      this.flights=res;
      if(this.flights.length==0){
        alert("no flights to view");

      }
      else{
        alert("Flights are available");
      }

    })

  }
  deleteFlight(num:any){
this.adminservice.deleteFlight(num).subscribe((res:any)=>{
    this.msg=res.msg;
    alert(this.msg);
   this. getAll();
});
  }
  block(num:any,status:any){
    alert(status)
    this.adminservice.changeFlyingStatus(num,status).subscribe((res:any)=>{
      this.msg=res.msg;
      alert(this.msg);
      this.getAll();

    });

  }
  reschedule(flightNum:any){
this.route.navigateByUrl("/adminsucess/reschedule")
  }

}
