import { Component, OnInit } from '@angular/core';
import { Flights } from 'src/app/model/flights';
import { FlyingDates } from 'src/app/model/flying-dates';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

msg:any="";
flightNumber:any="";
flightName:any="";
fplace:any="";
tplace:any="";
sdate:any="";
edate:any="";
dtime:any="";
atime:any="";
businessClassSeats:any="";
nonBusinessClassSeats:any="";
scheduledDays:any="";
ticketCost:any="";
status:any="";
instrumentUsed:any="";
rows:any="";
meal:any="";

fdays:Array<FlyingDates>=[];

  constructor(private admin:AdminServiceService) { }

  ngOnInit(): void {
  }
  addFlight(){
    
    let days=new FlyingDates();
    days.dates=this.scheduledDays;
 this.fdays.push(this.scheduledDays);
     let addFlight=new Flights();
    addFlight.flightNum=this.flightNumber
    addFlight.airlineName=this.flightName;
    addFlight.fromPlace=this.fplace;
    addFlight.toPlace=this.tplace;
    addFlight.startDate=this.sdate;
    addFlight.endDate=this.edate;
    addFlight.depTime=this.dtime;
    addFlight.arrTime=this.atime;
    addFlight.buisnesSeat=this.businessClassSeats;
    addFlight.nonBuisnessSeat=this.nonBusinessClassSeats;
    addFlight.instrument=this.instrumentUsed;
    addFlight.rows=this.rows;
    addFlight.mealType=this.meal;
    addFlight.flyingStatus=this.status;
    addFlight.flyingDates=this.fdays;
    addFlight.cost=this.ticketCost;
    if( !!this.flightNumber && !!this.flightName && !!this.fplace && !!this.fplace && !!this.tplace
      &&  !!this.sdate && !!this.edate && !!this.dtime && !!this.atime && !!this.businessClassSeats && !!this.nonBusinessClassSeats
      && !!this.scheduledDays && !!this.ticketCost && !!this.status && !!this.instrumentUsed && !!this.rows && !!this.meal){
    this.admin.addFlight(addFlight).subscribe((res:any)=>{
this.msg=res.msg;

alert(this.msg);
this.fdays=[];
    });
   
    

  }else{
    alert("Fill required data");
  }
  this.flightNumber=""
    this.flightName =""
    this.fplace =""
    this.fplace =""
    this.tplace=""
    this.sdate =""
    this.edate="" 
    this.dtime =""
    this.atime="" 
    this.businessClassSeats="" 
    this.nonBusinessClassSeats=""
    this.scheduledDays="" 
    this.ticketCost="" 
    this.status =""
    this.instrumentUsed=""
    this.rows =""
    this.meal=""

  }

}
