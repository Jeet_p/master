import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { FlightsComponent } from './components/flights/flights.component';
import { CreateComponent } from './components/create/create.component';

import { CuponsComponent } from './components/cupons/cupons.component';
import { RescheduleComponent } from './components/reschedule/reschedule.component';
import { ManagecuponsComponent } from './components/managecupons/managecupons.component';
import { ValidateadminGuard } from './guard/validateadmin.guard';
import { LogoutComponent } from './components/logout/logout.component';




export const routes: Routes = [
{ path:'',  component:HomeComponent,
children: [
  { path: "create", component: CreateComponent,canActivate: [ValidateadminGuard] },
  { path: "viewcupon", component: ManagecuponsComponent,canActivate: [ValidateadminGuard] },
  { path: "flights", component: FlightsComponent ,canActivate: [ValidateadminGuard]},
    { path: "cupons", component: CuponsComponent, canActivate: [ValidateadminGuard] },
    { path: "logout", component: LogoutComponent },
    { path: "reschedule", component: RescheduleComponent,canActivate: [ValidateadminGuard] }
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class AdminRoutingModule { }