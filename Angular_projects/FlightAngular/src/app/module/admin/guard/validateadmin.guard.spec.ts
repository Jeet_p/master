import { TestBed } from '@angular/core/testing';

import { ValidateadminGuard } from './validateadmin.guard';

describe('ValidateadminGuard', () => {
  let guard: ValidateadminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ValidateadminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
