import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './components/book/book.component';
import { SearchComponent } from './components/search/search.component';
import { HistoryComponent } from './components/history/history.component';
import { DownloadComponent } from './components/download/download.component';
import { FlightsComponent } from './components/flights/flights.component';
import { HomeComponent } from './components/home/home.component';
import { Router, RouterModule } from '@angular/router';
import {routes} from './user-routing.module';
import {  FormsModule } from '@angular/forms';
import { BookingdetailsComponent } from './components/bookingdetails/bookingdetails.component';
import { CancelticketComponent } from './components/cancelticket/cancelticket.component';
import { UserlogoutComponent } from './components/userlogout/userlogout.component';





@NgModule({
  declarations: [
    BookComponent,
    SearchComponent,
    HistoryComponent,
    DownloadComponent,
    FlightsComponent,
    HomeComponent,
    BookingdetailsComponent,
    CancelticketComponent,
    UserlogoutComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(routes),

  ]
})
export class UserModule { }
