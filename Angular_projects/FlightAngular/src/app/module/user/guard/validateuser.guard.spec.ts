import { TestBed } from '@angular/core/testing';

import { ValidateuserGuard } from './validateuser.guard';

describe('ValidateuserGuard', () => {
  let guard: ValidateuserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ValidateuserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
