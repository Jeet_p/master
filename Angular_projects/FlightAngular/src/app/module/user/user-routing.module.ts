import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BookComponent } from './components/book/book.component';
import { HistoryComponent } from './components/history/history.component';
import { SearchComponent } from './components/search/search.component';
import {DownloadComponent } from './components/download/download.component';
import { FlightsComponent } from '../admin/components/flights/flights.component';
import { BookingdetailsComponent } from './components/bookingdetails/bookingdetails.component';
import { CancelticketComponent } from './components/cancelticket/cancelticket.component';
import { ValidateuserGuard } from './guard/validateuser.guard';
import { LogoutComponent } from '../admin/components/logout/logout.component';
import { UserlogoutComponent } from './components/userlogout/userlogout.component';




export const routes: Routes = [
{ path:'',  component:HomeComponent,
children: [
  { path: "book", component: BookComponent,canActivate:[ValidateuserGuard] },
  { path: "history", component: HistoryComponent,canActivate:[ValidateuserGuard] },
  { path: "flights", component: FlightsComponent,canActivate:[ValidateuserGuard] },
    { path: "search", component: SearchComponent,canActivate:[ValidateuserGuard] },
    { path: "download", component: DownloadComponent,canActivate:[ValidateuserGuard] },
    { path: "booking", component: BookingdetailsComponent,canActivate:[ValidateuserGuard]},
    { path: "cancel", component: CancelticketComponent,},
    { path: "Logout", component: UserlogoutComponent}
    

    

]

}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }