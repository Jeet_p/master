import { Component, OnInit } from '@angular/core';
import { ChildActivationStart, Router } from '@angular/router';
import { Flights } from 'src/app/model/flights';
import { UserServiceService } from 'src/app/service/user-service.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  date:any="";
  fromPlace:any="";
  toPlace:any="";
  trip:any="";
  flights:Array<Flights>=[];
  msg:any="";



  constructor(private userservice:UserServiceService,private router:Router) { }
  
  ngOnInit(): void {
  }

  search(){
    if(!!this.date && !!this.fromPlace && !!this.toPlace){
    this.userservice.serachFlight(this.date,this.fromPlace,this.toPlace,this.trip)
    .subscribe((res:any)=>{
     debugger
        this.flights=res;
    
        if(this.flights.length>0){
          this.date=""
          this.fromPlace=""
          this.toPlace=""
         this. trip=""
        }else{
          this.msg=res.msg;
          alert(this.msg)
         this. date=""
         this. fromPlace=""
         this. toPlace=""
         this. trip=""
        }
      
     });
    }
     else{
      alert("Fill required data")
    }
  }

bookFlight(num:any){
this.router.navigateByUrl("/usersuccess/book")
}
}
