import { Component, OnInit } from '@angular/core';
import { iif } from 'rxjs';
import { FlightBook } from 'src/app/model/flight-book';
import { Passenger } from 'src/app/model/passenger';
import { UserServiceService } from 'src/app/service/user-service.service';
import { BookComponent } from '../book/book.component';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  pnr:any="";
  book:FlightBook=new FlightBook();
  msg:any="";
  constructor(public userservice:UserServiceService) { }

  ngOnInit(): void {
  }
  download(){
    if(!!this.pnr){
    this.validatePnr(this.pnr);
    }
    else{
      alert("Pnr should not be empty")
    }

    }
    validatePnr(pnr:any){

    this.userservice.validate(pnr).subscribe((res:any)=>{
    this.msg=res;
 alert(this.msg);
    if(this.msg==1){
     
      this.userservice.downloadTicket(pnr);
      this.pnr="";
    }
    else{
    
     this. msg="incorrect pnr number";
     alert(this.msg);

    }

    });
  }

}
