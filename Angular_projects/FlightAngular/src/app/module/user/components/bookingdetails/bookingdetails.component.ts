import { Component, OnInit } from '@angular/core';
import { BookingFlight } from 'src/app/model/booking-flight';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-bookingdetails',
  templateUrl: './bookingdetails.component.html',
  styleUrls: ['./bookingdetails.component.scss']
})
export class BookingdetailsComponent implements OnInit {
  pnr:any="";
  book:any;
msg:any="";
  constructor(private userService:UserServiceService) { }

  ngOnInit(): void {
  }

  getDetails(){
    if(!!this.pnr){
   
       this.userService.getBookingDeatils(this.pnr).subscribe((res:any)=>{
        this.book=res;
       
        if(!!this.book.name){;
         
        }
        else{
         this.msg=res.msg
         alert(this.msg)
        
        }
         });
        }else{
          alert("Pnr should not be empty")
        }
 
      }

}
  