import { Component, OnInit } from '@angular/core';
import { FlightBook } from 'src/app/model/flight-book';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  mail:any="";
  books:any=[];

  constructor(private userservice:UserServiceService) { }

  ngOnInit(): void {
  }
  getHistory(){
    if(!!this.mail){
    this.userservice.getHistory(this.mail).subscribe((res:any)=>{
     this.books=res;
     if(this.books.length==0){
       alert("No History available for given mail id")
       this.mail="";
     }
    });
    this.mail=""
  }
  else{
    alert("Email should not be empty")
  }

  }
 

}
