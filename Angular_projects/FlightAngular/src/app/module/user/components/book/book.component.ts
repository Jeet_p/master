import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/service/user-service.service';
import { FlightBook } from 'src/app/model/flight-book';
import { Passenger } from 'src/app/model/passenger';
import { BookingFlight } from 'src/app/model/booking-flight';
import { Cupon } from 'src/app/model/cupon';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  name:any;
  email:any;
  meal:any;
  seats:any;
  pname:any;
  gender:any;
  passengers:Array<Passenger>=[];
  count:number=0;
  msg:any;
  fnumber:any="";
  cupon:any="";
  cupons:Array<Cupon>=[];

  constructor(private userService:UserServiceService ) { }

  ngOnInit(): void {
    debugger
    this.userService.viewCupons().subscribe((res:any)=>{
     this.cupons=res;   
    });
  }
  addPassenger(){


   this. count++;

  if(this.count>this.seats){
    let passenger =new Passenger();
    passenger.name=this.pname;
    passenger.gender=this.gender
  
  
      this.passengers.push(passenger);
    alert("You have added passenger details for booked seat")
    this.msg="You have added passenger details for booked seat";

  }
  else{

   let passenger =new Passenger();
  passenger.name=this.pname;
  passenger.gender=this.gender


    this.passengers.push(passenger);
  }
  
  
}
  bookFlight(){
    debugger
    if(!!this.fnumber && !!this.name && !!this.email && !!this.meal && !! this.seats && !!this.pname && !!this.gender){
let book=new BookingFlight();
book.name=this.name;
book.mailId=this.email;
book.mealType=this.meal;
book.seats=this.seats;
book.passenger=this.passengers;
book.cuponCode=this.cupon.code;
     this.userService.bookFlight(book,this.fnumber).subscribe((res:any)=>{
       debugger
      this.msg=res.msg;
      alert(this.msg);
      this.passengers=[];
     });
     this.name="";
     this.meal="";
      this.seats="";
      this.pname="" ;
      this.gender="";
      this.email="";
      this.cupon="";
      this.fnumber=""
        }else{
      alert("Fill required input")
    }

  }

}
