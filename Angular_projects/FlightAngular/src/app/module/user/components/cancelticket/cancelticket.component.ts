import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-cancelticket',
  templateUrl: './cancelticket.component.html',
  styleUrls: ['./cancelticket.component.scss']
})
export class CancelticketComponent implements OnInit {
  pnr:any;
  msg:any;

  constructor(private userservice:UserServiceService) { }

  ngOnInit(): void {
  }
  cancel(){
    if(!!this.pnr){
    this.userservice.canceTicket(this.pnr).subscribe((res:any)=>{
      alert(res.msg);
      this.msg=res.msg;
      this.pnr="";
    });
  }
  else{
    alert("Pnr should not be empty");
    this.pnr="";
  }

  }

}
