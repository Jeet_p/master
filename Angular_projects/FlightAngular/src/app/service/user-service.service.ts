import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BookingFlight } from '../model/booking-flight';
import { FlightBook } from '../model/flight-book';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private baseUrl:string = "http://localhost:9191/app/v1.0/flight/user";
  
  private url :string="";
  private actualurl :string="";

  constructor(private http: HttpClient) { }
generateOtp(mail:any){
  this.url="/login";
this.actualurl=this.baseUrl+this.url+"/"+mail;
console.log(this.actualurl);
return this.http.get(this.actualurl);

}
userValidate(mail:any,otp:any){
  this.url="/validate";
  this.actualurl=this.baseUrl+this.url+"/"+mail+"/"+otp;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);
}

getHistory(mail:any){
  console.log(mail);
  this.url="/history";
  this.actualurl=this.baseUrl+this.url+"/"+mail;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);
}
getBookingDeatils(pnr:any){

  this.url="/details";
  this.actualurl=this.baseUrl+this.url+"/"+pnr;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);

}
canceTicket(pnr:any){
  this.url="/cancel";
  this.actualurl=this.baseUrl+this.url+"/"+pnr;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);
}
downloadTicket(pnr:any){
 
  this.url="/download";
  this.actualurl=this.baseUrl+this.url+"/"+pnr;
  window.location.href = this.actualurl;
  // console.log(this.actualurl);
  // return this.http.get(this.actualurl);
}
validate(pnr:any){
  this.url="/chackpnr";
  this.actualurl=this.baseUrl+this.url+"/"+pnr;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);
}

serachFlight(startDate:any,fromPlace:any,toPlace:any,trip:any){
  this.url="/search";
  this.actualurl=this.baseUrl+this.url+"/"+startDate+"/"+fromPlace+"/"+toPlace+"/"+trip;
  console.log(this.actualurl);

  return this.http.get(this.actualurl);

}
bookFlight(flight:BookingFlight,num:any){
  this.url="/book";
  this.actualurl=this.baseUrl+this.url+"/"+num;
  console.log(this.actualurl);
  return this.http.post(this.actualurl,flight);
}
viewCupons(){
  this.url="/viwecupons";
  this.actualurl=this.baseUrl+this.url;
  console.log(this.actualurl);
  return this.http.get(this.actualurl);
  
}

}
