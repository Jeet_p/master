import { Injectable } from '@angular/core';
import { Admin } from '../model/admin';
import { HttpClient } from '@angular/common/http';
import { Cupon } from '../model/cupon';
import { Flights } from '../model/flights';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {
  private host:string = "http://localhost:9191/app/v1.0/flight/admin/authenticate";
  private baseUrl:string="http://localhost:9191/app/v1.0/flight/admin";
  private url :string="";
  private actualurl :string="";

  constructor(private http: HttpClient) { }

authenticate(admin:Admin){
  console.log(admin.userName,admin.password);
return this.http.post(this.host,admin);
}
getAllflights(){
this.url="/flights";
this.actualurl=this.baseUrl+this.url
console.log(this.actualurl);
return this.http.get(this.actualurl);

}
deleteFlight(num:any){

  this.url="/delete"+"/"+num;
  this.actualurl=this.baseUrl+this.url
  console.log(this.actualurl);
  return this.http.delete(this.actualurl);

}
createCupon(cupon:Cupon){
  this.url="/createcupon";
  this.actualurl=this.baseUrl+this.url;
  return this.http.post(this.actualurl,cupon);
}

changeFlyingStatus(num:any,status:any){
  this.url="/change";

  if(status=="flying"){
    alert("in flying block")
    alert(status)
    this.actualurl=this.baseUrl+this.url+"/"+num+"/"+"blocked";
   
  } 
  else if(status=="blocked")
  {
    alert("in blocked blocked")
    alert(status)
    this.actualurl=this.baseUrl+this.url+"/"+num+"/"+"flying";
   
  }
  return this.http.get(this.actualurl);


}
viewCupons(){
  this.url="/cupons";
  this.actualurl=this.baseUrl+this.url;
  return this.http.get(this.actualurl);
}


// updateFlight(){
//   alert("service")
//   this.url="/cupons";
//   this.actualurl=this.baseUrl+this.url;
//   return this.http.get(this.actualurl);
// }
  updateFlight(flight:Flights,num:any){
    console.log("admin update")
  
   this.url="/reschedule";
 this.actualurl=this.baseUrl+this.url+"/"+num;
  alert(this.actualurl) 
  return this.http.put(this.actualurl,flight);

 }
 update(){
   alert("service update");
 }
 addFlight(flight:Flights){
 
  this.url="/create";
this.actualurl=this.baseUrl+this.url;
 alert(this.actualurl) 
 return this.http.post(this.actualurl,JSON.stringify(flight),{
  headers: {
    'Content-Type': 'application/json'
 }
});





 }
}
