import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon'
import {MatButtonModule}  from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeadercomponentComponent } from './components/headercomponent/headercomponent.component';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';
import { UserloginComponent } from './components/userlogin/userlogin.component';
import { UserModule } from './module/user/user.module';
import { CustommaterialmoduleModule } from './module/custommaterialmodule/custommaterialmodule.module';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { FlightBook } from './model/flight-book';

@NgModule({
  declarations: [
    AppComponent,
   HeadercomponentComponent,
 
    AdminloginComponent,
    UserloginComponent,
   
  
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustommaterialmoduleModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    HttpClientModule,

   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
