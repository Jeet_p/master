package com.cts.flight.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.flight.admin.exception.CuponNotFoundException;
import com.cts.flight.admin.exception.UserNotFoundException;
import com.cts.flight.admin.model.Admin;
import com.cts.flight.admin.model.Cupon;
import com.cts.flight.admin.model.ErrorResponse;
import com.cts.flight.admin.model.FlightCreate;
import com.cts.flight.admin.service.AdminService;

import io.swagger.annotations.Api;
import io.swagger.models.Response;


@RestController
@RequestMapping	
@Api
public class AdminController {
	@Autowired
	private AdminService adminService;

	//@PostMapping("/create")
	public ResponseEntity<String> createAdmin(@RequestBody Admin admin) {
		Admin ad = adminService.createAdmin(admin);
		if (ad != null) {
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}

		else {
			return new ResponseEntity<String>("Admin not created", HttpStatus.OK);
		}

	}

	
	  @PostMapping("/validate") 
	  public ResponseEntity<ErrorResponse> validateAdmin(@RequestBody Admin admin) throws UserNotFoundException {
	  
	  String validMsg = adminService.isValidAdmin(admin); return new
	  ResponseEntity<ErrorResponse>(new ErrorResponse(validMsg, 200),
	  HttpStatus.OK);
	  
	  }
	
	@PostMapping(path="/create")
	public ResponseEntity<ErrorResponse> createFlights(@RequestBody FlightCreate flight ) {
		String msg=adminService.saveFlightDetails(flight);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.CREATED);
		
		
	}
	@GetMapping("/change/{number}/{status}")
	public ResponseEntity<ErrorResponse> changeFlyingStatus(@PathVariable int number,@PathVariable String status){
		String msg=adminService.changeFlyingStatus(status, number);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
	@PutMapping("/reschedule/{number}")
	public ResponseEntity<ErrorResponse> rescheduleFlight(@RequestBody FlightCreate flight,@PathVariable int number){
		System.out.println("admin reschedule");
		String msg=adminService.updateFlightTime(flight, number);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
	@GetMapping("/flights")
	public ResponseEntity<List<FlightCreate>> getAllFlights() throws UserNotFoundException {
		List<FlightCreate> flights=adminService.getAllFlights();
		return new ResponseEntity<List<FlightCreate>>(flights,HttpStatus.FOUND);
		
	}
	@PutMapping("/updateflights/{number}")
	public ResponseEntity<ErrorResponse> updateFlights(@RequestBody FlightCreate flight,@PathVariable int number){
		String msg=adminService.updateFlight(flight, number);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
	@DeleteMapping("/delete/{number}")
	public ResponseEntity<ErrorResponse> deleteFlightByNumber(@PathVariable int number){
		String msg=adminService.deleteFlight(number);
		
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
    @PostMapping("/createcupon")
	public ResponseEntity<ErrorResponse> createCupons(@RequestBody Cupon cupon) {
    	System.out.println("angular->cupon"+cupon.getCode());
    	String msg="";
    	msg=adminService.addCupon(cupon);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.CREATED);
		
	}
	/*
	 * @PutMapping("/managecupon/{code}") public ResponseEntity<ErrorResponse>
	 * manageCupon(@RequestBody Cupon cupon,@PathVariable String code) throws
	 * CuponNotFoundException{ String msg=adminService.modiFyCupon(cupon, code);
	 * return new ResponseEntity<ErrorResponse>(new
	 * ErrorResponse(msg,200),HttpStatus.OK);
	 * 
	 * }
	 */
	
    @GetMapping("/cupons")
    public ResponseEntity<List<Cupon>> getCupons() throws CuponNotFoundException{
    	List<Cupon> allCupon=adminService.allCupons();
		return new ResponseEntity<List<Cupon>>(allCupon,HttpStatus.OK);
    	
    }
    @GetMapping("/getcupon/{code}")
    public ResponseEntity<ErrorResponse> getCuponByCode(@PathVariable String code) throws CuponNotFoundException {
    	Cupon cupn=adminService.getCuponDetails(code);
    	
		return new ResponseEntity(cupn,HttpStatus.OK);
    	
    	
    }
    
	}


