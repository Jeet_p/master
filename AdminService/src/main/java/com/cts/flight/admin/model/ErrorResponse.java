package com.cts.flight.admin.model;

public class ErrorResponse {
	private int statusCode;
	private String msg;
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ErrorResponse(String msg, int statusCode) {
		super();
		this.statusCode = statusCode;
		this.msg = msg;
	}
	public ErrorResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
