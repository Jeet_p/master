package com.cts.flight.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cts.flight.admin.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, String> {
	
	  @Query("SELECT COUNT(*) FROM Admin WHERE userName= :userName AND password= :password"
	  ) public int validateAdmin(@Param("userName") String
	  userName,@Param("password") String password);
	 
	//@Query("SELECT COUNT(*) FROM Admin WHERE userName= :userName AND password= :password")
	//public int validateAdmin(@Param("userName") String userName,@Param("password") String password);
	
	public Admin findByUserName(String userName);


}
