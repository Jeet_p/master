package com.cts.flight.admin.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cts.flight.admin.model.Admin;
import com.cts.flight.admin.repository.AdminRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private AdminRepository adminRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // should load from database
        // comparing if user is "demo"
		Optional<Admin> opt=adminRepo.findById(username);
		Admin adm;
		String name="";
		String pswd="";
		if(opt.isPresent()) {
			adm=opt.get();
		 name=adm.getUserName();
		 pswd=adm.getPassword();
		 System.out.println(name+"----->"+pswd);
		}
		if (name.equals(username)) {
			// return new User(name, "{noop}admin", new ArrayList<>());
			return new User(name, pswd, new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
}