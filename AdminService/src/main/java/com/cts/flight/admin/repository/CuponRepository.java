package com.cts.flight.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cts.flight.admin.model.Cupon;
@Repository
public interface CuponRepository  extends JpaRepository<Cupon, String> {

}
