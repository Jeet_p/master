package com.cts.flight.admin.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;



public class FlightCreate implements Serializable {

	private int flightNum;
	
	private String airlineName;
	
	private String fromPlace;
	
	private String toPlace;

	private String startDate;
	
	private String endDate;
	
	private String depTime;
	
	private String arrTime;
 
	List<flyingDates> flyingDates;
	private String instrument;
	
	private int buisnesSeat;
	
	private int nonBuisnessSeat;
	private double cost;

	private String flyingStatus;
	
	private int rows;
	private String mealType;
	
	public int getFlightNum() {
		return flightNum;
	}
	public void setFlightNum(int flightNum) {
		this.flightNum = flightNum;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getFromPlace() {
		return fromPlace;
	}
	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}
	public String getToPlace() {
		return toPlace;
	}
	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getDepTime() {
		return depTime;
	}
	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public List<flyingDates> getFlyingDates() {
		return flyingDates;
	}
	public void setFlyingDates(List<flyingDates> flyingDates) {
		this.flyingDates = flyingDates;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public int getBuisnesSeat() {
		return buisnesSeat;
	}
	public void setBuisnesSeat(int buisnesSeat) {
		this.buisnesSeat = buisnesSeat;
	}
	public int getNonBuisnessSeat() {
		return nonBuisnessSeat;
	}
	public void setNonBuisnessSeat(int nonBuisnessSeat) {
		this.nonBuisnessSeat = nonBuisnessSeat;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getFlyingStatus() {
		return flyingStatus;
	}
	public void setFlyingStatus(String flyingStatus) {
		this.flyingStatus = flyingStatus;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getMealType() {
		return mealType;
	}
	public void setMealType(String mealType) {
		this.mealType = mealType;
	}
	@Override
	public String toString() {
		return "FlightCreate [flightNum=" + flightNum + ", airlineName=" + airlineName + ", fromPlace=" + fromPlace
				+ ", toPlace=" + toPlace + ", startDate=" + startDate + ", endDate=" + endDate + ", depTime=" + depTime
				+ ", arrTime=" + arrTime + ", flyingDates=" + flyingDates + ", instrument=" + instrument
				+ ", buisnesSeat=" + buisnesSeat + ", nonBuisnessSeat=" + nonBuisnessSeat + ", cost=" + cost
				+ ", flyingStatus=" + flyingStatus + ", rows=" + rows + ", mealType=" + mealType + "]";
	}
	public FlightCreate(int flightNum, String airlineName, String fromPlace, String toPlace, String startDate,
			String endDate, String depTime, String arrTime, List<com.cts.flight.admin.model.flyingDates> flyingDates,
			String instrument, int buisnesSeat, int nonBuisnessSeat, double cost, String flyingStatus, int rows,
			String mealType) {
		super();
		this.flightNum = flightNum;
		this.airlineName = airlineName;
		this.fromPlace = fromPlace;
		this.toPlace = toPlace;
		this.startDate = startDate;
		this.endDate = endDate;
		this.depTime = depTime;
		this.arrTime = arrTime;
		this.flyingDates = flyingDates;
		this.instrument = instrument;
		this.buisnesSeat = buisnesSeat;
		this.nonBuisnessSeat = nonBuisnessSeat;
		this.cost = cost;
		this.flyingStatus = flyingStatus;
		this.rows = rows;
		this.mealType = mealType;
	}
	public FlightCreate() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}