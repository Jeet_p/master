package com.cts.flight.admin.exception;

public class CuponNotFoundException extends Exception {

	public CuponNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CuponNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CuponNotFoundException(String message, Exception cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CuponNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CuponNotFoundException(Exception cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
