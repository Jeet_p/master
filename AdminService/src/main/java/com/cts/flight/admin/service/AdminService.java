package com.cts.flight.admin.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cts.flight.admin.exception.CuponNotFoundException;
import com.cts.flight.admin.exception.UserNotFoundException;
import com.cts.flight.admin.model.Admin;
import com.cts.flight.admin.model.Cupon;
import com.cts.flight.admin.model.ErrorResponse;
import com.cts.flight.admin.model.FlightCreate;
import com.cts.flight.admin.repository.AdminRepository;
import com.cts.flight.admin.repository.CuponRepository;

@Service
public class AdminService {
	@Autowired
	private AdminRepository adminRepo;
	@Autowired
	private RestTemplate template;
	@Autowired
	private CuponRepository cuponRepo;

	public Admin createAdmin(Admin admin) {
		return adminRepo.save(admin);

	}

	public String isValidAdmin(Admin admin) throws UserNotFoundException {

		String msg = "";
		System.out.println(admin.getUserName());
		System.out.println((admin.getPassword()));
		int count = adminRepo.validateAdmin(admin.getUserName(), admin.getPassword());
		System.out.println(count);
		if (count != 0) {
			msg = "successfully loged in as admin";
		} else {
			throw new UserNotFoundException("You are not authorized as Admin,check with Admin people");
		}

		return msg;
	}

	public String saveFlightDetails(FlightCreate flight) {
		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/create";
		// HttpEntity<FlightCreate> body=new RequestEntity<FlightCreate>(flight);
		HttpEntity<FlightCreate> entity = new HttpEntity<FlightCreate>(flight);
		ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.POST, entity,
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		ErrorResponse response = res.getBody();
		if (response.getStatusCode() == 200) {
			msg = response.getMsg();
		} else {
			msg = "unable to create Flight";
		}
		return msg;

	}

	public String changeFlyingStatus(String status, int number) {
		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/status/" + number + "//" + status;
		ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		if (res.getBody().getStatusCode() == 200) {
			msg = res.getBody().getMsg();
		} else {
			msg = "Unable to change flying status for flight " + number;
		}
		return msg;
	}

	public String updateFlightTime(FlightCreate flight, int number) {
		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/reschedule/" + number;
		HttpEntity<FlightCreate> entity = new HttpEntity<FlightCreate>(flight);
		ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.PUT, entity,
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		System.out.println("adm service->" + res.getBody().getMsg() + "----" + res.getBody().getStatusCode());
		System.out.println(res.getBody().toString());
		if (res.getBody().getStatusCode() == 200) {
			System.out.println("inside iffff");
			msg = res.getBody().getMsg();
			return msg;
		}
		return msg = "Unnnnnnnnable to reschedule flight for " + number;

	}

	public List<FlightCreate> getAllFlights() throws UserNotFoundException {
		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/flights";

		ResponseEntity<List<FlightCreate>> res = template.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FlightCreate>>() {
				});
		if (res.getStatusCode() == HttpStatus.FOUND) {
			List<FlightCreate> flights = res.getBody();
			return flights;
		} else {
			msg = "Flighs not there";
			throw new UserNotFoundException(msg);
		}
		// return null;

	}

	public String updateFlight(FlightCreate flight, int number) {

		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/update/" + number;
		HttpEntity<FlightCreate> entity = new HttpEntity<FlightCreate>(flight);

		ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.PUT, entity,
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		if (res.getBody().getStatusCode() == 200) {
			msg = res.getBody().getMsg();

		}
		return msg;
	}

	public String deleteFlight(int number) {
		String msg = "";
		String url = "http://localhost:9091/app/v1.0/flight/airlline/delete/" + number;

		ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.DELETE, null,
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		if (res.getBody().getStatusCode() == 200) {
			msg = res.getBody().getMsg();
			return msg;
		} else {
			msg = "Flighs not there to delete";
			return msg;
			// throw new UserNotFoundException(msg);
		}
	}

	public String addCupon(Cupon cupon) {
		String msg = "";
		if (cupon.getCode() != null && cupon.getDiscount() != 0 && !cupon.getCode().isEmpty()) {
			Optional<Cupon> opt=cuponRepo.findById(cupon.getCode());
			if (opt.isPresent()) {
				cuponRepo.save(cupon);
				msg = "Cupon is  updated with cupon id " + cupon.getCode();
			}
			else {
				cuponRepo.save(cupon);
			}
		} else {
			msg = "Cupon code and discount amount should not be empty";
		}
		return msg;

	}

	/*
	 * public String modiFyCupon(Cupon cupon, String code) throws
	 * CuponNotFoundException { String msg = ""; if (code != null &&
	 * !code.isEmpty()) { Cupon existcupon = cuponRepo.getById(code); if (existcupon
	 * != null) { existcupon.setDiscount(cupon.getDiscount());
	 * cuponRepo.save(existcupon); msg = "cupon amount is update for " + code; }
	 * else { msg = "cupon is not available to update"; throw new
	 * CuponNotFoundException(msg);
	 * 
	 * } } else { msg = "Cupon code should not be empty"; } return msg; }
	 */

	public List<Cupon> allCupons() throws CuponNotFoundException{
		List<Cupon> cupons=cuponRepo.findAll();
		if(cupons!=null) {
			return cupons;
		}
		else {
			throw new CuponNotFoundException("No cupons are there to view");
		}
		
	}
	public Cupon  getCuponDetails(String code) throws CuponNotFoundException  {
		String msg="";
		if(code!=null && !code.isEmpty()) {
			
			Optional<Cupon> opt=cuponRepo.findById(code);
		if(opt.isPresent()) {
			Cupon cupn=opt.get();
			return cupn;
		}
		else {
			msg="cupon not available with code "+ code;
			throw new CuponNotFoundException(msg);
		}
		}
		else {
			msg="cupon code should not be empty";
			throw new CuponNotFoundException(msg);
		}
	}
}
