package com.cts.flight.admin.aop;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.cts.flight.admin.exception.CuponNotFoundException;
import com.cts.flight.admin.exception.UserNotFoundException;
import com.cts.flight.admin.model.ErrorResponse;

@ControllerAdvice
public class ExceptionHandler {
@org.springframework.web.bind.annotation.ExceptionHandler(UserNotFoundException.class)

public ResponseEntity<ErrorResponse> handlerForUserNotFoundException(UserNotFoundException e){
	return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(),404),HttpStatus.NOT_FOUND);
	
}
@org.springframework.web.bind.annotation.ExceptionHandler(CuponNotFoundException.class)

public ResponseEntity<ErrorResponse> handlerForCuponNotFoundException(Exception e){
	return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(),404),HttpStatus.NOT_FOUND);
	
}


}
