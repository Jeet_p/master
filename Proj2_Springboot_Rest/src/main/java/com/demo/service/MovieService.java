package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.Movies;
import com.demo.repo.MoviRepository;
@Service
public class MovieService {
	@Autowired
	private MoviRepository moveRepository;
	
	public Movies saveMovies(Movies movie) {
		return moveRepository.save(movie);
	}

}
