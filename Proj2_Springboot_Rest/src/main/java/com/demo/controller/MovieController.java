package com.demo.controller;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.Movies;
import com.demo.service.MovieService;
@RestController
@RequestMapping("/app/v1/movie")
public class MovieController {
@Autowired
private MovieService movieService;
@PostMapping
public Movies saveMovies(@RequestBody Movies movie) throws ParseException {
	System.out.println(movie);
		/*
		 * SimpleDateFormat df=new SimpleDateFormat("yyyy/mm/dd"); java.util.Date
		 * date=df.parse(movie.getDate()); Date dt=
		 */
	//return null;
	return movieService.saveMovies(movie);
}
@GetMapping("/get")
public String greet() {
	return "welcome";
}
}
