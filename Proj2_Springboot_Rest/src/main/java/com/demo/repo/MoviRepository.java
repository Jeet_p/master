package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.Movies;

public interface MoviRepository extends JpaRepository<Movies,Integer> {

}
