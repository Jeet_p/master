package com.cts.flight.admin;

import org.bouncycastle.crypto.PasswordConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class AdminServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminServiceApplication.class, args);
		
	       PasswordEncoder encoder = new BCryptPasswordEncoder();

	        String encoded = encoder.encode("admin");
	        // $2a$10$7dyBJ0GDBJgqqV4PWzNPaOy1q.LEy8znVHpOkeNVHLvlTP9Ty.696
	        System.out.println("encode pswd->"+encoded);

	        System.out.println(encoder.matches("abc", encoded));
	        System.out.println(encoder.matches("demo@123", encoded));
	        System.out.println(encoder.matches("demo#124", encoded));


	        System.out.println(encoder.matches("admin", encoded));
	}
	

}
