package com.cts.flight.admin.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

public class flyingDates  implements Serializable{
	 @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private String dates;

	/*
	 * public int getId() { return id; } public void setId(int id) { this.id = id; }
	 */
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public flyingDates( String dates) {
		super();
		//this.id = id;
		this.dates = dates;
	}
	public flyingDates() {
		super();
		// TODO Auto-generated constructor stub
	}

}
