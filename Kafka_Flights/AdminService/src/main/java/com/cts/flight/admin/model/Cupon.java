package com.cts.flight.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cupons")
public class Cupon {
	@Id
	private String code;
	private int discount;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}


	
	public Cupon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cupon(String cuponId, int discount) {
		super();
		
		this.code = code;
		this.discount = discount;
	}

	
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	

}
