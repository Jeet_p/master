package com.cts.flight.user.service;

public interface IOtpService {
	public int generateOTP(String key);
	public int getOtp(String key);
	public void clearOTP(String key);
	boolean validateOtp(int otp1, int otp2);

}
