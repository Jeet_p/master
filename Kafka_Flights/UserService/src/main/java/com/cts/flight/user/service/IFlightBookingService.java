package com.cts.flight.user.service;

import java.util.List;

import com.cts.flight.user.exceptions.CancelTimeExceded;
import com.cts.flight.user.exceptions.DataNotFoundException;
import com.cts.flight.user.exceptions.UserNotFoundException;
import com.cts.flight.user.model.Cupon;
import com.cts.flight.user.model.FlightBook;

public interface IFlightBookingService {
	public String bookFlight(int flightNum,FlightBook book) throws DataNotFoundException;
	public List<FlightBook> getBookedFlightsByMailId(String mailId);
	public FlightBook getTicketDetailsByPnr(String pnr) throws UserNotFoundException;
	public String cancelTickets(String pnr) throws CancelTimeExceded, UserNotFoundException;
	public List<Cupon> viewCupon() throws DataNotFoundException;
	public double applyCupons(FlightBook book) throws DataNotFoundException;
	public double getFlightCost(FlightBook book) throws DataNotFoundException;
	public int validPnr(String pnr);

}
