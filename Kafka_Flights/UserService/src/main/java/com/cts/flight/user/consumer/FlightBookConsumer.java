package com.cts.flight.user.consumer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.cts.flight.user.model.FlightBook;

@Configuration
@EnableKafka

public class FlightBookConsumer {

	  @Bean
	    public ConsumerFactory<String, FlightBook> flightConsumerFactory() {
		
	    	
	    	 JsonDeserializer<FlightBook> deserializer = new JsonDeserializer<>(FlightBook.class);
	    	    deserializer.setRemoveTypeHeaders(false);
	    	    deserializer.addTrustedPackages("com.cts.flight.user");
	    	    deserializer.setUseTypeMapperForKey(true);

	    	    Map<String, Object> config = new HashMap<>();

	    	    config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	    	    config.put(ConsumerConfig.GROUP_ID_CONFIG, "group_two");
	    	    config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
	    	    config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
	    	    config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
	    	    config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer);

	    	    return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), deserializer);
	    	}
	    

	    @Bean
	    public ConcurrentKafkaListenerContainerFactory<String, FlightBook> flightKafkaListenerFactory() {
	        ConcurrentKafkaListenerContainerFactory<String, FlightBook> factory = new ConcurrentKafkaListenerContainerFactory<>();
	        factory.setConsumerFactory(flightConsumerFactory());
	        return factory;
	    }
	
}
