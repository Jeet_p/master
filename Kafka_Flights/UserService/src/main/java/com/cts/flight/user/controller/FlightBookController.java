package com.cts.flight.user.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cts.flight.user.exceptions.CancelTimeExceded;
import com.cts.flight.user.exceptions.DataNotFoundException;
import com.cts.flight.user.exceptions.OldDateException;
import com.cts.flight.user.exceptions.UserNotFoundException;
import com.cts.flight.user.model.Cupon;
import com.cts.flight.user.model.ErrorResponse;
import com.cts.flight.user.model.FlightBook;
import com.cts.flight.user.model.FlightSearch;
import com.cts.flight.user.service.IFlightBookingService;

import io.swagger.annotations.Api;

@RestController
@Api
public class FlightBookController {
	@Autowired
	private IFlightBookingService service;
	@Autowired
	private RestTemplate template;

	@PostMapping("/book/{flightnum}")
	public ResponseEntity<ErrorResponse> bookFlight(@PathVariable int flightnum, @RequestBody FlightBook book)
			throws DataNotFoundException {
		String msg = service.bookFlight(flightnum, book);
		System.out.println(book.getPassenger());
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200), HttpStatus.OK);

	}

	@GetMapping("/history/{mailId}")
	public ResponseEntity<List<FlightBook>> bookingHistoryByMailId(@PathVariable String mailId) {
		List<FlightBook> books = service.getBookedFlightsByMailId(mailId);
		return new ResponseEntity<List<FlightBook>>(books, HttpStatus.OK);

	}

	@GetMapping("/search/{startDate}/{fromPlace}/{toPlace}/{trip}")
	public ResponseEntity<List<FlightSearch>> searchFlight(@PathVariable String startDate,
			@PathVariable String fromPlace, @PathVariable String toPlace, @PathVariable String trip)
			throws ParseException, OldDateException, DataNotFoundException {

		System.out.println("user search=>" + startDate + "->" + fromPlace + "->" + toPlace);
		List<FlightSearch> flights = new ArrayList<FlightSearch>();

		String[] s = startDate.split("-");
		for (int i = 0; i < s.length; i++) {
			System.out.println(s[i]);
		}
		String newDate = s[2] + "-" + s[1] + "-" + s[0];
		System.out.println(newDate);
		// to avoid old date
		Date current = new Date();
		if (compareDate(startDate, current)==true) {

			String baseUrl = "http://localhost:9191/app/v1.0/flight/airlline";
			baseUrl = baseUrl + "//search" + "//" + newDate + "//" + fromPlace + "//" + toPlace + "//" + trip;
			ResponseEntity<List<FlightSearch>> resp = template.exchange(baseUrl, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<FlightSearch>>() {
					});
			if (resp.getBody() != null && !resp.getBody().isEmpty()) {
				System.out.println(resp.getBody());

				flights = resp.getBody();
			}
			else {
				throw new DataNotFoundException("No flights available for your searching details");
			}
		} else {
			throw new OldDateException("you cant select old date");
		}
		return new ResponseEntity<List<FlightSearch>>(flights, HttpStatus.OK);
		/*
		 * else { throw new
		 * DataNotFoundException("Flight not available on searching details"); }
		 */

	}

	public Boolean compareDate(String date1, Date date2) {
		String dt1[] = date1.split("-");
		int d1 = Integer.parseInt(dt1[0] + dt1[1] + dt1[2]);
		boolean flag = true;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = dateFormat.format(date2);
		System.out.println(strDate);
		String dt2[] = strDate.split("-");
		int d2 = Integer.parseInt(dt2[0] + dt2[1] + dt2[2]);
		System.out.println("Converted String: d2 " + d2);
		System.out.println("Converted String: d1 " + d1);
		if (d1 < d2) {
			flag = false;
		} else if (d1 > d2 || d1 == d2) {
			flag = true;
		}
		System.out.println(flag);
		return flag;
	}
	@Cacheable(key="#pnr",value="user")
	@GetMapping("/details/{pnr}")
	public ResponseEntity<FlightBook> getBookingDetailsByPnr(@PathVariable String pnr) throws UserNotFoundException {
		System.out.println("PNR->" + pnr);
		System.out.println("Local time:"+LocalDateTime.now());
		FlightBook book = service.getTicketDetailsByPnr(pnr);
		return new ResponseEntity<FlightBook>(book, HttpStatus.OK);

	}

	@GetMapping("/cancel/{pnr}")
	public ResponseEntity<ErrorResponse> cancelTickets(@PathVariable String pnr)
			throws CancelTimeExceded, UserNotFoundException {
		String msg;
		try {
			msg = service.cancelTickets(pnr);
		} catch (NullPointerException n) {
			msg = "Flight is not ready to flight with issue,contact help line";
			throw new UserNotFoundException(msg);
		}
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200), HttpStatus.OK);

	}

	@GetMapping("/download/{pnrnum}")
	public ResponseEntity<InputStreamResource> downloadPdf(@PathVariable String pnrnum) throws DataNotFoundException {
		String path = "C:\\Users\\User\\Desktop\\Jeet.cts\\UserService\\src\\main\\resources\\templates\\tickets\\"
				+ pnrnum + ".pdf";
		File file = new File(path);

		if (file.exists()) {
			HttpHeaders respHeaders = new HttpHeaders();
			MediaType mediaType = MediaType.parseMediaType("application/pdf");
			respHeaders.setContentType(mediaType);
			respHeaders.setContentLength(file.length());
			respHeaders.setContentDispositionFormData("attachment", file.getName());
			InputStreamResource isr;
			try {
				isr = new InputStreamResource(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				String msg = "ticket deatils are not there to downloade,incorrect PNR number";
				throw new DataNotFoundException(msg);
			}
			return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
		} else {
			String msg = "ticket deatils are not there to downloade,incorrect PNR number";
			throw new DataNotFoundException(msg);
		}

		// return new
		// ResponseEntity<InputStreamResource>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	// return null;

	@GetMapping("/viwecupons")
	public ResponseEntity<List<Cupon>> vieCupon() throws DataNotFoundException {
		List<Cupon> cupons = service.viewCupon();
		return new ResponseEntity<List<Cupon>>(cupons, HttpStatus.OK);

	}

	@GetMapping(value = "/chackpnr/{pnr}", produces = "application/json")
	public int isValidPnr(@PathVariable String pnr) {
		int msg;
		msg = service.validPnr(pnr);
		return msg;

	}

}
