package com.cts.flight.user.model;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.Table;
@Entity
@Table(name="booking_details")
public class FlightBook{
	@Id

	@Column(name="pnr_number")
	private String pnrNum;
	@Column(name="name")
	private String name;
	@Column(name="mail_id")
	private String mailId;
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "passenger_details",joinColumns =@JoinColumn(name= "pnrNum"))
	private List<Passenger> passenger;
	private String mealType;
	private int seats;

	private int flightNum;
	private String flightName;
	private double cost;
	private String cuponCode;

	public String getPnrNum() {
		return pnrNum;
	}
	public void setPnrNum(String pnrNum) {
		this.pnrNum = pnrNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public List<Passenger> getPassenger() {
		return passenger;
	}
	public void setPassenger(List<Passenger> passenger) {
		this.passenger = passenger;
	}
	public String getMealType() {
		return mealType;
	}
	public void setMealType(String mealType) {
		this.mealType = mealType;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public int getFlightNum() {
		return flightNum;
	}
	public void setFlightNum(int flightNum) {
		this.flightNum = flightNum;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public FlightBook() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FlightBook(String pnrNum, String name, String mailId, List<Passenger> passenger, String mealType, int seats,
			int flightNum, String flightName, double cost) {
		super();
		this.pnrNum = pnrNum;
		this.name = name;
		this.mailId = mailId;
		this.passenger = passenger;
		this.mealType = mealType;
		this.seats = seats;
		this.flightNum = flightNum;
		this.flightName = flightName;
		this.cost = cost;
	}
	public String getCuponCode() {
		return cuponCode;
	}
	public void setCuponCode(String cuponCode) {
		this.cuponCode = cuponCode;
	}
	
}
