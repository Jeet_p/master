package com.cts.flight.user.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class FSwaggerConfig {
	@Bean
	public Docket swagerConfig() {
		return new Docket(DocumentationType.SWAGGER_2).
				select().//paths(PathSelectors.ant("com.cts.flight")).build();
				
				
				apis(RequestHandlerSelectors.basePackage("com.cts.flight.user")).build();
	}

}
