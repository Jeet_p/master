package com.cts.flight.user.kafka.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.cts.flight.user.model.FlightBook;
import com.cts.flight.user.repository.BookingFlightRepository;

@Service
public class BookFlightKafkaListener {
	
	  private static final String TOPIC = "flight_book";
	    @Autowired
	    private BookingFlightRepository repo;

	   @KafkaListener(topics = TOPIC, groupId="group_two", containerFactory = "flightKafkaListenerFactory")

	    public void consumeJson(@Payload FlightBook flight, @Headers MessageHeaders headers)  {
	    	System.out.println("Airline consumer");
	    	repo.save(flight);
	        System.out.println("Consumed JSON Message: " + flight);
	    }


}
