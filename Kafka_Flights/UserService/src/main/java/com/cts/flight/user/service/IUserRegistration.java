package com.cts.flight.user.service;

import com.cts.flight.user.model.UserRegd;

public interface IUserRegistration {
	public UserRegd createUser(UserRegd user);
	public boolean  checkUserMailId(String mail);

}
