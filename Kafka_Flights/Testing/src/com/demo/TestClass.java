package com.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestClass {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		
		 
		   Date current = new Date();
		    String myFormatString = "dd-mm-yyyy";
		    SimpleDateFormat df = new SimpleDateFormat(myFormatString);
		    Date givenDate = df.parse("15-02-2022");
		    Long l = givenDate.getTime();
		    //create date object
		    Date next = new Date(l);
		    System.out.println(next);
		    //compare both dates
		    if(next.after(current) || (next.equals(current))){
		        System.out.println("The date is future day");
		    } else {

		        System.out.println("The date is older than current day");
		    }

	}

}
