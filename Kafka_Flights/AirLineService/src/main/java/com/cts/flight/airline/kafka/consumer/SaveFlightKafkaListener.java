/*package com.cts.flight.airline.kafka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.cts.flight.airline.exceptions.UserExistException;
import com.cts.flight.airline.model.FlightCreate;
import com.cts.flight.airline.service.IAirlineService;

@Service
public class SaveFlightKafkaListener{
    private static final String TOPIC = "flight_topic";
    @Autowired
    private IAirlineService airService;

   @KafkaListener(topics = TOPIC, groupId="group_one", containerFactory = "flightKafkaListenerFactory")

    public void consumeJson(@Payload FlightCreate flight, @Headers MessageHeaders headers) throws UserExistException {
    	System.out.println("Airline consumer");
    	airService.createFlight(flight);
        System.out.println("Consumed JSON Message: " + flight);
    }

}*/
