package com.cts.flight.airline.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cts.flight.airline.exceptions.FlightNotFoundException;
import com.cts.flight.airline.exceptions.UserExistException;
import com.cts.flight.airline.model.ErrorResponse;
import com.cts.flight.airline.model.FlightCreate;
import com.cts.flight.airline.service.IAirlineService;

import io.swagger.annotations.Api;

@RestController
@Api
public class AirlineController {
	@Autowired
	private IAirlineService airService ;
	@PostMapping("/create")
	public ResponseEntity<ErrorResponse> createFlight(@RequestBody FlightCreate flight) throws UserExistException{
	String msg=	airService.createFlight(flight);
	
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200),HttpStatus.CREATED);
		
	}
	@GetMapping("/status/{flightnum}/{status}")
	public ResponseEntity<ErrorResponse>  blockAirLine(@PathVariable int flightnum,@PathVariable String status) throws FlightNotFoundException{
	 String msg= airService.changeFlyingStatus(flightnum, status);
	 
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
	@PutMapping("/update/{flightNum}")
	public ResponseEntity<ErrorResponse> updateFlight(@RequestBody FlightCreate flight,@PathVariable int flightNum) throws FlightNotFoundException{
		String msg=airService.updateFlight(flight, flightNum);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200),HttpStatus.OK);
		
	}
	@DeleteMapping("/delete/{flightnum}")
	public ResponseEntity<ErrorResponse> deleteFlight(@PathVariable int flightnum) throws FlightNotFoundException{
		String msg=airService.deleteFlightByNumber(flightnum);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg, 200),HttpStatus.OK);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<FlightCreate>> getAllFlights() throws FlightNotFoundException{
		return new ResponseEntity<List<FlightCreate>>(airService.getAllFlights(), HttpStatus.OK);
	}
	@GetMapping("/search/{startDate}/{fromPlace}/{toPlace}/{trip}")
	public ResponseEntity<List<FlightCreate>> searchFlightes(@PathVariable String startDate,@PathVariable String fromPlace,@PathVariable String toPlace,@PathVariable String trip ) throws ParseException{
		System.out.println("air controller angular->"+startDate+"->"+fromPlace+"->+"+toPlace+"->"+trip);
		
		List<FlightCreate> listFlights=airService.searchFlight(startDate, fromPlace, toPlace,trip);
		return new ResponseEntity<List<FlightCreate>>(listFlights,HttpStatus.OK);
		
	}
	@GetMapping("/getname/{number}")
	public String getFlightNameBynumber(@PathVariable int number) throws FlightNotFoundException {
		return airService.getFlightName(number);
		
	}
	@GetMapping("/flightdeatils/{number}")
	public ResponseEntity<FlightCreate> getFlightDetails(@PathVariable int number) throws FlightNotFoundException {
		FlightCreate flight=airService.getFlightDetails(number);
		return new ResponseEntity<FlightCreate>(flight,HttpStatus.OK);
		
		
	}
	@PutMapping("/reschedule/{number}")
	public ResponseEntity<ErrorResponse> resheduleFlight(@RequestBody FlightCreate flight,@PathVariable int number) throws FlightNotFoundException{
		String msg=airService.reschedule(flight, number);
		System.out.println("airline controller->"+msg);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
		
	}
	@GetMapping("/flights")
	public ResponseEntity<List<FlightCreate>> getAllFlights1() throws FlightNotFoundException {
		System.out.println("AWS airline_flights");
		List<FlightCreate> listFlights=airService.getAllFlights();
		return new ResponseEntity<List<FlightCreate>>(listFlights,HttpStatus.OK);
		
	}
	
}
