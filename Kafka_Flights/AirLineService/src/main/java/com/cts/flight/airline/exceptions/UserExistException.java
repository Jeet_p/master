package com.cts.flight.airline.exceptions;

public class UserExistException extends Exception {

	public UserExistException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserExistException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public UserExistException(String msg,Exception e) {
		super(msg, e);
		// TODO Auto-generated constructor stub
	}

	public UserExistException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public UserExistException(Exception e) {
		super(e);
		// TODO Auto-generated constructor stub
	}
	

}
