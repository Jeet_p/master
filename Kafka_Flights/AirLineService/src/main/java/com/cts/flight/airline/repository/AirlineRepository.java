package com.cts.flight.airline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cts.flight.airline.model.FlightCreate;

public interface AirlineRepository extends JpaRepository<FlightCreate, Integer> {
	public FlightCreate findByFlightNum(int flightNum);
	public List<FlightCreate> findByStartDateAndFromPlaceAndToPlaceAndFlyingStatus(String startDate,String fromplace,String toPlace,String status);
	

}
