package com.cts.flight.kafka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.cts.flight.kafka.model.FlightCreate;

@Service
public class KafkaService {
	@Autowired
	private KafkaTemplate<String, FlightCreate> kafka;
	private String topic = "flight_topic";

	public String sendToTopic(FlightCreate flight) {
		 Message<FlightCreate> message = MessageBuilder
		            .withPayload(flight)
		            .setHeader(KafkaHeaders.TOPIC, topic)
		            .build();
		   // this.kafkaTemplate.send(message);
		System.out.println("flight->"+flight);
		String msg="";
		System.out.println("kafkaTemplate->"+kafka);
		System.out.println("kafka save flight");
		//kafka.sendDefault(topic, flight);
		kafka.send(topic,flight);
		msg="Airlne created  successfully";
	
		/*
		 * catch(Exception e) {
		 * 
		 * msg="Data unable to publish"; }
		 */
		return msg;
		
	}

}
