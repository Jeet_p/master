package com.cts.flight.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaFlightServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaFlightServicesApplication.class, args);
	}

}
