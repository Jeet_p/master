package com.cts.flight.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.flight.kafka.model.ErrorResponse;
import com.cts.flight.kafka.model.FlightCreate;
import com.cts.flight.kafka.service.KafkaService;

@RestController
@RequestMapping()
public class KafkaController {
	@Autowired
	private KafkaService kafkaService;
	@PostMapping("/save")
	public ResponseEntity<ErrorResponse> sendData(FlightCreate flight) {
		String msg=kafkaService.sendToTopic(flight);
		System.out.println(flight);
		System.out.println("toic--->");
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(msg,200),HttpStatus.OK);
	}

}
