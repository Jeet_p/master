package com.cts.flight.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cts.flight.kafka.model.ErrorResponse;
import com.cts.flight.kafka.model.FlightCreate;

@Service
public class SaveFlightKafkaListener{
    private static final String TOPIC = "flight_topic";
    @Autowired
    private RestTemplate template;


   @KafkaListener(topics = TOPIC, groupId="group_one", containerFactory = "flightKafkaListenerFactory")

    public void consumeJson(@Payload FlightCreate flight, @Headers MessageHeaders headers)  {
    	System.out.println("Airline consumer");
    	//airService.createFlight(flight);
    	SaveFlight(flight);
        System.out.println("Consumed JSON Message: " + flight);
    }
   
   public String SaveFlight(FlightCreate flight) {
   String msg = "";
	String url = "http://localhost:9191/app/v1.0/flight/airlline/create";
	// HttpEntity<FlightCreate> body=new RequestEntity<FlightCreate>(flight);
	HttpEntity<FlightCreate> entity = new HttpEntity<FlightCreate>(flight);
	ResponseEntity<ErrorResponse> res = template.exchange(url, HttpMethod.POST, entity,
			new ParameterizedTypeReference<ErrorResponse>() {
			});
	ErrorResponse response = res.getBody();
	if (response.getStatusCode() == 200) {
		msg = response.getMsg();
	} else {
		msg = "unable to create Flight";
	}
	return msg;
   }


}
