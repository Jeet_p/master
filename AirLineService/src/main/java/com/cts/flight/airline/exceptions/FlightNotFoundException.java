package com.cts.flight.airline.exceptions;

public class FlightNotFoundException extends Exception{

	public FlightNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FlightNotFoundException(String msg, Exception e) {
		super(msg, e);
		// TODO Auto-generated constructor stub
	}

	public FlightNotFoundException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public FlightNotFoundException(Exception e) {
		super(e);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
