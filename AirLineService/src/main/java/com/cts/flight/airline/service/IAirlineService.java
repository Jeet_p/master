package com.cts.flight.airline.service;

import java.text.ParseException;
import java.util.List;

import com.cts.flight.airline.exceptions.FlightNotFoundException;
import com.cts.flight.airline.exceptions.UserExistException;
import com.cts.flight.airline.model.FlightCreate;

public interface IAirlineService {
	public String createFlight(FlightCreate flight) throws UserExistException;
	public String updateFlight(FlightCreate flight,int flightNum) throws FlightNotFoundException;
	//public String blockFlight(int flightNum);
	//public String blockFlight(int flightNum, String status) throws FlightNotFoundException;
	 public String changeFlyingStatus(int flightNum, String status) throws FlightNotFoundException;
	 public String deleteFlightByNumber(int flightNum) throws FlightNotFoundException;
	 public List<FlightCreate> getAllFlights() throws FlightNotFoundException;
	 public List<FlightCreate> searchFlight(String startDate,String fromPlace,String toPlace,String status) throws ParseException;
	 public String getFlightName(int num) throws FlightNotFoundException;
	 public FlightCreate getFlightDetails(int num) throws FlightNotFoundException;
	public String reschedule(FlightCreate flight, int flightNumber) throws FlightNotFoundException;
	public List<FlightCreate> getAllFllights()throws FlightNotFoundException;
}
