package com.cts.flight.airline.aop;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.cts.flight.airline.exceptions.FlightNotFoundException;
import com.cts.flight.airline.exceptions.UserExistException;
import com.cts.flight.airline.model.ErrorResponse;

@ControllerAdvice
public class ExceptionControllerAdvice {
	@ExceptionHandler(UserExistException.class)
	public ResponseEntity<ErrorResponse> handlerUserExist(UserExistException e){
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(), 409),HttpStatus.CONFLICT);
		
		
	}
	
	@ExceptionHandler(FlightNotFoundException.class)
	public ResponseEntity<ErrorResponse> handlerFlightNotFoundException(FlightNotFoundException e){
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage(), 409),HttpStatus.NOT_FOUND);
		
		
	}


}
