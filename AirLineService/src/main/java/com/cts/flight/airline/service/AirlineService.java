package com.cts.flight.airline.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cts.flight.airline.exceptions.FlightNotFoundException;
import com.cts.flight.airline.exceptions.UserExistException;
import com.cts.flight.airline.model.FlightCreate;
import com.cts.flight.airline.repository.AirlineRepository;

@Service
public class AirlineService implements IAirlineService {
	@Autowired
	private AirlineRepository airRepo;

	@Override
	public String createFlight(FlightCreate flight) throws UserExistException {
		// TODO Auto-generated method stub
		String msg = "";
		if (airRepo.findByFlightNum(flight.getFlightNum()) == null) {
			FlightCreate newF = airRepo.save(flight);
			if (newF != null) {
				msg = newF.getAirlineName() + " Flight created with flight number " + newF.getFlightNum();
			} else {
				msg = "unable to create flight";
			}

		} else {
			msg = "Flight with number " + flight.getFlightNum() + " is exist";
			throw new UserExistException(msg);
		}
		return msg;
	}

	@Override
	public String updateFlight(FlightCreate flight, int flightNum) throws FlightNotFoundException {
		String msg = "";
		// TODO Auto-generated method stub
		FlightCreate existFlight = airRepo.findByFlightNum(flightNum);
		if (existFlight != null && existFlight.getFlyingStatus().equalsIgnoreCase("flying")) {
			if (airRepo.save(flight) != null) {
				;
				msg = "Aire Line " + flight.getAirlineName() + " and flight number " + flight.getFlightNum()
						+ " updated";
			} else {
				msg = "unable to update flight deatils for flight number " + flightNum;
			}

		} else {
			msg = "Flight " + flightNum + "not there or flight is blocked";
			throw new FlightNotFoundException(msg);
		}
		return msg;

	}

	@Override
	public String changeFlyingStatus(int flightNum, String status) throws FlightNotFoundException {
		String msg = "";
		// TODO Auto-generated method stub
		FlightCreate existFlight = airRepo.findByFlightNum(flightNum);
		if (existFlight != null && existFlight.getFlyingStatus().equalsIgnoreCase("flying")
				&& status.equalsIgnoreCase("blocked")) {
			existFlight.setFlyingStatus(status);
			airRepo.save(existFlight);
			msg = "Flight " + flightNum + " blocked";
		} else if (existFlight != null && existFlight.getFlyingStatus().equalsIgnoreCase("blocked")
				&& status.equalsIgnoreCase("flying")) {
			existFlight.setFlyingStatus(status);
			airRepo.save(existFlight);
			msg = "Flight " + flightNum + " is ready to fly";
		}

		else if (existFlight != null && existFlight.getFlyingStatus().equalsIgnoreCase("blocked")
				&& status.equalsIgnoreCase("blocked")) {
			msg = "Flight number " + flightNum + "  already blocked";

			// throw new FlightNotFoundException(msg);
		} else if (existFlight != null && existFlight.getFlyingStatus().equalsIgnoreCase("flying")
				&& status.equalsIgnoreCase("flying")) {
			msg = "Flight number " + flightNum + "  is already ready to fly stus";
			// throw new FlightNotFoundException(msg);
		} else {
			msg = "Flight number " + flightNum + "  is not created ";
			throw new FlightNotFoundException(msg);
		}
		return msg;
	}

	@Override
	@Transactional
	public String deleteFlightByNumber(int flightNum) throws FlightNotFoundException {
		String msg = "";
		// TODO Auto-generated method stub
		if (airRepo.findByFlightNum(flightNum) != null) {
			airRepo.deleteById(flightNum);
			msg = "Flight " + flightNum + "deleted";
		} else {
			msg = "Flight " + flightNum + "not available to delete";
			throw new FlightNotFoundException(msg);
		}
		return msg;
	}

	@Override
	public List<FlightCreate> getAllFlights() throws FlightNotFoundException {
		// TODO Auto-generated method stub
		List<FlightCreate> flights = airRepo.findAll();
		if (flights != null) {
			return flights;
		}

		else {
			String msg = "no flights to show";
			throw new FlightNotFoundException(msg);
		}

	}

	@Override
	public List<FlightCreate> searchFlight(String startDate, String fromPlace, String toPlace,String wayStatus) throws ParseException {
		// TODO Auto-generated method stub
		String status="flying";
		//List<FlightCreate> flights=airRepo.findByStartDateAndFromPlaceAndToPlaceAndFlyingStatus(startDate, fromPlace, toPlace,status);
		if(wayStatus.equalsIgnoreCase("two")) {
			System.out.println("two");
			String date=startDate;
		   //	String date="11-09-2021";
		   	SimpleDateFormat sdf=new SimpleDateFormat("dd-mm-yyyy");
			Date date1=sdf.parse(date);
			Calendar cal=Calendar.getInstance();
			cal.setTime(date1);
			cal.add(Calendar.DAY_OF_MONTH, 2);
			System.out.println(sdf.format(cal.getTime()));
		String roundDate=sdf.format(cal.getTime());
		List<FlightCreate> flights_one=airRepo.findByStartDateAndFromPlaceAndToPlaceAndFlyingStatus(startDate, fromPlace, toPlace,status);
		List<FlightCreate> flights_two=airRepo.findByStartDateAndFromPlaceAndToPlaceAndFlyingStatus(roundDate, toPlace, fromPlace,status);
		flights_one.addAll(flights_two);
		
		return flights_one;
		}
		else {
			List<FlightCreate> flights=airRepo.findByStartDateAndFromPlaceAndToPlaceAndFlyingStatus(startDate, fromPlace, toPlace,status);
			return flights;
		}
		
		//return flights;
	}

	@Override
	public String getFlightName(int num) throws FlightNotFoundException {
		// TODO Auto-generated method stub
		FlightCreate flight=airRepo.findByFlightNum(num);
		System.out.println("flight statsu->"+ flight.getFlyingStatus());
		if(flight!=null && flight.getFlyingStatus().equalsIgnoreCase("flying")) {
			String name=flight.getAirlineName();
			return name;
		}
		else {
			String msg="Flight not available or blocked to fly with number "+ num;
			throw new FlightNotFoundException(msg);
		}
		
	}

	@Override
	public FlightCreate getFlightDetails(int num) throws FlightNotFoundException {
		// TODO Auto-generated method stub
		
		FlightCreate flight=airRepo.findByFlightNum(num);
		System.out.println("flight statsu->"+ flight.getFlyingStatus());
		if(flight!=null && flight.getFlyingStatus().equalsIgnoreCase("flying")) {
		
			return flight;
		}
		else {
			String msg="Flight not available or blocked to fly with number "+ num;
			throw new FlightNotFoundException(msg);
		}
		
		//return flight;
	}

	@Override
	@Transactional
	public String reschedule(FlightCreate flight,int flightNumber) throws FlightNotFoundException {
		String msg="";
		// TODO Auto-generated method stub
		FlightCreate fl=airRepo.findByFlightNum(flightNumber);
		if(fl!=null) {
			if(flight.getStartDate()!=null) {
				System.out.println("air serv-->1");
				fl.setStartDate(flight.getStartDate());
				
			}
			if(flight.getEndDate()!=null) {
				System.out.println("air serv-->11");
				fl.setEndDate(flight.getEndDate());
				
			}
			 if(flight.getArrTime()!=null) {
				System.out.println("air serv-->2");
				fl.setArrTime(flight.getArrTime());
				
			}
			if(flight.getDepTime()!=null) {
				System.out.println("air serv-->3");
				fl.setDepTime(flight.getDepTime());
				
			}
			airRepo.save(fl);
			msg="Flight "+ flightNumber + "is rescheduled";
		}else{
			msg="Flight " + flightNumber + "is not avaialble to reschedule";
			throw new FlightNotFoundException(msg);
		}
		return msg;
	}

	@Override
	public List<FlightCreate> getAllFllights() throws FlightNotFoundException {
		// TODO Auto-generated method stub
		List<FlightCreate> allFlights=airRepo.findAll();
		if(allFlights!=null) {
			return allFlights;
		}
		else {
			throw new FlightNotFoundException("No flights are there to view");
		}
	
	}
}
