package com.cts.flight.airline.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Embeddable
//@Table(name="flying_dates")
public class flyingDates  implements Serializable{

	private String dates;

	/*
	 * public int getId() { return id; } public void setId(int id) { this.id = id; }
	 */
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public flyingDates(int id, String dates) {
		super();
		//this.id = id;
		this.dates = dates;
	}
	public flyingDates() {
		super();
		// TODO Auto-generated constructor stub
	}

}
