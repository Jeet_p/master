package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Proj1Springboot11Application {

	public static void main(String[] args) {
		SpringApplication.run(Proj1Springboot11Application.class, args);
	}

}
